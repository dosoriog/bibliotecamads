$(".btn-cargardocumento").on("click", function(){
              var id = $('#documento').val();
              var swedit = $('#swedit').val();
              if((!id)||(id=='')){
                        alert('Debe seleccionar un documento pendiente por egreso o pago.');
                        $('#documento').focus();
              }else{
                      $.ajax({
                              type:'POST',
                              url:base_url+ "buscardoc",
                              dataType: "json",
                              data:{id:id},
                              success:function(data){
                                  if(data.estado == 'ok'){
                                      $('#identificacion').val(data.registro.tipodoc+' - '+data.registro.identificacion);
                                      $('#persona').val(data.registro.persona);
                                      $('#docm').val(data.registro.codigo+' - '+data.registro.documento);
                                      $('#numero').val(data.registro.numero);
                                      $('#valor').val(data.registro.total);
                                      $('#fecha').val(data.registro.fecha);
                                      $('#iddoc').val(data.registro.id);
                                      $('#tdoc').val(data.registro.codigo);
                                      $('#doc').val(data.registro.documento);
                                      $('#num').val(data.registro.numero);
                                      $('#ccon').val(data.registro.codconcepto);
                                      $('#fec').val(data.registro.fecha);
                                      $('#val').val(data.registro.valor);
                                      $('#tidp').val(data.registro.idpersona);
                                      $('#tide').val(data.registro.tipodoc);
                                      $('#tper').val(data.registro.codtipo);
                                      $('#ide').val(data.registro.identificacion);
                                      $('#nom').val(data.registro.persona);
                                  }else{
                                      alert("Error en la conexion...");
                                  }
                              }
                          });
              }
});
