
$( "#FormDatReg" ).validate( {
	rules: {
		tipo: {
			required: true,
			number: true
		},
		nombre: {
			required: true,
			minlength: 2,
			maxlength: 200
	  },
		categoria: {
			required: true,
			number: true
		},
		presentacion: {
			required: true,
			number: true
		},
		unidadmedida: {
			required: true,
			number: true
		},
		marcas: {
			required: true,
			number: true
		},
		existencias: {
			required: true,
			number: true
		},
		minexistencias: {
			required: true,
			number: true
		},
		maxexistencias: {
			required: true,
			number: true
		},
		descuento: {
			required: true,
			number: true
		},
		descuentoprincipal: {
			required: true,
		},
		impuestoiva: {
			required: true,
			number: true
		},
		ultcompra: {
			required: true,
			number: true
		},
		valor1: {
			required: true,
			number: true
		},
		valor2: {
			required: true,
			number: true
		},
		valor3: {
			required: true,
			number: true
		},
	},
	messages: {
		tipo: {
			required: "Selecione el tipo del inventario.",
			number: "Selecione el tipo del inventario.",
		},
		nombre: {
			required: "Ingrese un nombre valido para el item de inventario. 2 a 200 caracteres.",
			minlength: "Ingrese un nombre valido para el item de inventario. Minimo 2 caracteres",
			maxlength: "Ingrese un nombre valido para el item de inventario. Maximo 200 caracteres"
		},
		categoria: {
			required: "Selecione la categoria.",
			number: "Selecione el tipo del inventario.",
		},
		presentacion: {
			required: "Selecione la presentacion.",
			number: "Selecione el tipo del inventario.",
		},
		unidadmedida: {
			required: "Selecione la unidad de medida.",
			number: "Selecione el tipo del inventario.",
		},
		marcas: {
			required: "Selecione la marca.",
			number: "Selecione el tipo del inventario.",
		},
		existencias: {
			required: "Ingrese existencias.",
			number: "Ingrese existencias.",
		},
		minexistencias: {
			required: "Ingrese minimo de existencias.",
			number: "Ingrese minimo de existencias.",
		},
		maxexistencias: {
			required: "Ingrese maximo de existencias.",
			number: "Ingrese maximo de existencias.",
		},
		descuento: {
			required: "Ingrese un porcentaje de descuento.",
			number: "Ingrese un porcentaje de descuento.",
		},
		descuentoprincipal: {
			required: "Selecione tipo de descuento.",
			number: "Selecione tipo de descuento.",
		},
		impuestoiva: {
			required: "Ingrese porcentaje IVA.",
			number: "Ingrese porcentaje IVA.",
		},
		ultcompra: {
			required: "Ingrese ultimo valor de compra.",
			number: "Ingrese ultimo valor de compra.",
		},
		valor1: {
			required: "Ingrese valor 1.",
			number: "Ingrese valor 1.",
		},
		valor2: {
			required: "Ingrese valor 2.",
			number: "Ingrese valor 2.",
		},
		valor3: {
			required: "Ingrese valor 3.",
			number: "Ingrese valor 3.",
		},
	},
	errorElement: "em",
	errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".form-group" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}

					$(element).parents('.form-group').append(error);
	},
	success: function ( label, element ) {
		// Add the span element, if doesn't exists, and apply the icon classes to it.
		if ( !$( element ).next( "span" )[ 0 ] ) {
			$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
		}

	},
	highlight: function ( element, errorClass, validClass ) {
							$( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
							$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
	},
	unhighlight: function (element, errorClass, validClass) {
				$( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
				$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
	}
} );
