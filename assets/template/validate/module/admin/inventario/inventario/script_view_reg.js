function upload_image(){//Funcion encargada de enviar el archivo via AJAX
      $(".upload-msg").text('Cargando...');
      var inputFileImage = document.getElementById("fileToUpload");
      var file = inputFileImage.files[0];
      var data = new FormData();
      data.append('fileToUpload',file);

      /*jQuery.each($('#fileToUpload')[0].files, function(i, file) {
        data.append('file'+i, file);
      });*/

      $.ajax({
        url: base_url+"upload",        // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: data, 			  // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data)   // A function to be called if request succeeds
        {
           result = data.split("|");
           if(result[0]=="OK"){
                 $("#img_value").val(result[1]);
             }
           else{
               if(!result[1]){
                     $("#modal-default .modal-body").html(result[1]);
                     $("#modal-default").modal('show');
               }
            }
        }
      });

    }
$(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});

    function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#fileToUpload").change(function(){
		    readURL(this);
		});

	});
