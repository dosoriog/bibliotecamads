$('#fecha').datepicker({
  autoclose: true,
  todayBtn: true,
  showInputs: false,
  todayHighlight: true,
  format: format_fechas,
})

$(".btn-addinev").on("click", function(){
    $("#modal-selectinevnt").modal('show');
});

$(".btn-agregainv").on("click", function(){
    $('#inventario').val($('#inventario_aux').val());
    $('#cantidad').val($('#cantidad_aux').val());
    $( "#frmentradas" ).submit();
});
function eliminaIteminevntario(idinv,nominv,cantinv) {
      $('#idinv').val(idinv);
      $('#inteminv_eli').val(nominv);
      $('#inteminv_eli').prop('disabled', true);
      $('#cantinv_eli').val(cantinv);
      $('#cantinv_eli').prop('disabled', true);
      $("#mi-modaleliminaitem").modal('show');
    //  $( "#frmeliminainev" ).submit();
}
function conf_eliminaIteminevntario() {
     $( "#frmeliminainev" ).submit();
}
function actualizarIteminevntario(idinv,nominv,cantinv) {
      $('#idinv_act').val(idinv);
      $('#inteminv_act').val(nominv);
      $('#inteminv_act').prop('disabled', true);
      $('#cantinv_act').val(cantinv);
    //  $('#cantinv_act').prop('disabled', true);
      $("#mi-modalactualiza").modal('show');
    //  $( "#frmeliminainev" ).submit();
}
function conf_actualizarIteminevntario() {
     $('#cantinv').val($('#cantinv_act').val());
     $( "#frmactualizainev" ).submit();
}
$(".btn-agregainv").on("click", function(){
    $('#numero_add').val($('#numero').val());
    $('#fecha_add').val($('#fecha').val());
    $('#documento_add').val($('#documento').val());
    $('#personao_add').val($('#personao').val());
    $('#bodegao_add').val($('#bodegao').val());
    $('#personad_add').val($('#personad').val());
    $('#bodegad_add').val($('#bodegad').val());
    $('#idinv_add').val($('#inventario_aux').val());
    $('#cantinv_add').val($('#cantidad_aux').val());
    $('#descripcion_add').val($('#descripcion').val());
    $( "#frmadditem" ).submit();
});
$(".btn-gfcompra").on("click", function(){
    var id = $(this).val();
    var numero = $('#numero').val();
    var fecha = $('#fecha').val();
    var documento = $('#documento').val();
    var personao = $('#personao').val();
    var bodegao = $('#bodegao').val();
    var personad = $('#personad').val();
    var bodegad = $('#bodegad').val();
    var numreg = $('#numreg').val();
    var swedit1 = $('#swedit1').val();
    if(!numero){ numero = 0; }
    if((!documento)||(documento==0)){
              $('#modal-default .modal-body').html('<h4>Seleccione documento de la entrada de inventario.</h4>');
              $('#documento').focus();
    }else{

          if((!personao)||(personao==0)){
                 $("#modal-default .modal-body").html('<h4>Seleccione persona de origen.</h4>');
                 $('#personao').focus();
          }else{

                if((!bodegao)||(bodegao==0)){
                          $("#modal-default .modal-body").html('<h4>Seleccione bodega de origen.</h4>');
                          $('#bodegao').focus();
                   }else{

                               if((!personad)||(personad==0)){
                                      $("#modal-default .modal-body").html('<h4>Seleccione persona de destino.</h4>');
                                      $('#personad').focus();
                               }else{

                                     if((!bodegad)||(bodegad==0)){
                                               $("#modal-default .modal-body").html('<h4>Seleccione bodega de destino.</h4>');
                                               $('#bodegad').focus();
                                        }else{

                                          if((!numreg)||(numreg==0)){
                                                 $("#modal-default .modal-body").html('<h4>Debe agregar el detalle de la entrada de inventario.</h4>');
                                                 $('#persona').focus();
                                           }else{
                                                     $( "#frmentradas" ).submit();
                                                      /* $("#modal-default .modal-body").html('<h4>Guardando entrada de inventario...</h4>');
                                                       $.ajax({
                                                           url: base_url + "insert",
                                                           data: {id:id,numero:numero, fecha:fecha, documento:documento,personao:personao,bodegao:bodegao,personad:personad,bodegad:bodegad,swedit1:swedit1},
                                                           type:"POST",
                                                           success:function(resp){
                                                                self.location.href = resp;
                                                           }

                                                       });*/
                                               }


                                            }

                                 }
                       }
            }
      }

});

var modalConfirm = function(callback){

          $("#btnconfirma").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });

          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
};

modalConfirm(function(confirm){
            if(confirm){
              var id      = $('#id').val();

              var est     = $('#estentrada').val();
              if((!id)||(id=='')){
                        alert('Debe seleccionar una entrada en estado <GRABADO> pendiente por <CONFIRMAR>.');
              }else {
                         if((!est)||(est=='')||(est!='3')){
                              alert('Estado no valido. Debe seleccionar una entrada en estado <GRABADO>.');
                        }else{
                                      $( "#frmconfirmaestentrada" ).submit();
                              }
                    }
            }
  });
  var modalElimina = function(callback){
            $("#btnelimina").on("click", function(){
                $("#mi-modalelimina").modal('show');
            });
            $("#modale-btn-si").on("click", function(){
              callback(true);
              $("#mi-modalelimina").modal('hide');
            });

            $("#modale-btn-no").on("click", function(){
              callback(false);
              $("#mi-modalelimina").modal('hide');
            });
  };

  modalElimina(function(confirm){
          if(confirm){
                var id      = $('#id').val();
                var est     = $('#estentrada').val();
                if((!id)||(id=='')){
                          alert('Debe seleccionar una entrada en estado <GRABADO>.');
                }else {
                           if((!est)||(est=='')||(est!='3')){
                                alert('Estado no valido. Debe seleccionar una entrada en estado <GRABADO>.');
                          }else{
                                        $( "#frmeliminaentrada" ).submit();
                                }
                      }
          }
        });
function conf_cancelar() {
     $( "#frmcancelar" ).submit();
}


$(".btn-gfcancelar").on("click", function(){
    if(($('#sw_carguedoc_can').val()=="SI")&&($('#swedit2').val()==1)){
         $("#mi-modalcancelarentrada").modal('show');
    }
    else{
         $( "#frmcancelar" ).submit();
    }
});

function btn_regresarindex(){
      sw_g = $('#sw_guardar').val();
      if(sw_g=="SI"){
             $("#confi-guardar").modal('show');
      }else{
             self.location.href = base_url;
        }
}

function btn_imprimir(id){
        $.ajax({
            url: base_url + "imprimir",
            data: {id:id},
            type:"POST",
            success:function(resp){
                  newWin = window.open("");
                  newWin.document.write(resp);
                  //newWin.print();
                  //newWin.close();
            }

        });
}
