

	$( "#FormDatReg" ).validate( {
				rules: {
						persona: {
								 required: true,
							 },
					 inventario: {
								 required: true,
							 },
					 	cantidad: {
								required: true,
								minlength: 1,
								maxlength: 100
							},
							valor: {
								required: true,
								number: true,
							},
							fecha: {
								required: true,
							},
							numero: {
								required: true,
								minlength: 1,
								maxlength: 20,
							},
				},
				messages: {
					persona: {
						required: "Seleccione un proveedor para la factura",
					},
					inventario: {
								required: "Seleccione un producto/servicio para el detalle",
							},
					cantidad: {
						required: "Ingrese cantidad del inventario.",
						minlength: "Ingrese una cantidad mayor a 1.",
						maxlength: "Ingrese una cantidad mayor a 1."
					},
					valor: {
						required: "Ingrese valor del inventario.",
					},
					fecha: {
						required: "Seleccione una fecha de la factura.",
					},
					numero: {
						required: "Ingrese # factura. Entre 1 y 20 caracteres.",
						minlength: "Ingrese # factura. Entre 1 y 20 caracteres.",
						maxlength: "Ingrese # factura. Entre 1 y 20 caracteres.",
					},
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
								// Add the `help-block` class to the error element
								error.addClass( "help-block has-feedback" );
              //  alert(element.prop( "type" ));
								// Add `has-feedback` class to the parent div.form-group
								// in order to add icons to inputs

								element.parents( ".form-group" ).addClass( "has-feedback" );
              //  element.parents( ".form-group" ).addClass( "has-error" );

								if ( element.prop( "type" ) === "checkbox" ) {
									error.insertAfter( element.parent( "label" ) );
								} else {
									    if ( element.prop( "type" ) === "select-one" ) {
									         error.insertAfter( element.next( "span" )[ 0 ] );
												}
											else {
												   error.insertAfter( element );
											}
								}

								// Add the span element, if doesn't exists, and apply the icon classes to it.
								if ( !element.next( "span" )[ 0 ] ) {
									if ( element.prop( "type" ) === "select-one" ) {
								     	$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element.parents( ".form-group" ) );
									 }
									 else {
										  $( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
									 }
								}

								//element.parents( ".form-group" ).addClass( "has-error" );
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
							$( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
						//	$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function (element, errorClass, validClass) {
							$( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
							//$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			} );
