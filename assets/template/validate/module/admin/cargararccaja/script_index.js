
function showdetalleplatilla(id){
    $( "#idshow" ).val(id);
    $( "#formshowdet" ).submit();
}

var oFileIn, evento;

function guardarEvento(evt) {
         evento = evt;
}

$("#exportar_excel").click(function () {
        var elt = document.getElementById('Exportar_a_Excel');
        var wb = XLSX.utils.table_to_book(elt, { sheet: "Sheet JS" });
        return XLSX.writeFile(wb, 'test.xlsx');
});

function doit(type, fn, dl) {
          var elt = document.getElementById('data-table');
          var wb = XLSX.utils.table_to_book(elt, { sheet: "Sheet JS" });
          return dl ?
            XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
            XLSX.writeFile(wb, 'test.xlsx');
}

function eliminarArchivos(archivo) {
           $.ajax({
               url: base_url + 'eliminar_archivo',
               type: 'POST',
               timeout: 10000,
               data: {archivo: archivo},
               error: function() {
                   //mostrarRespuesta('Error al intentar eliminar el archivo.', false);
               },
               success: function(respuesta) {
                   if (respuesta == 1) {
                       //mostrarRespuesta('El archivo ha sido eliminado.', true);
                   } else {
                      // mostrarRespuesta('Error al intentar eliminar el archivo.', false);
                   }

               }
           });
 }

 function subirArchivos() {
             $("#archivo").upload( base_url + 'subir_archivo',
             {
                 nombre_archivo: ''
             },
             function(respuesta) {
                 //Subida finalizada.
                 if (respuesta != 0) {
                      //  alert('El archivo ha sido subido correctamente.'+respuesta);
                      $("#cuerpo_tabla").html('');
                      $.ajax({
                          url: base_url1 + 'archivos_subidos/'+respuesta,
                          dataType: 'text',
                     }).done(successFunction);

        //eliminarArchivos(respuesta);
                 } else {
                     alert('El archivo NO se ha podido subir.');
                 }
             }, function(progreso, valor) {
                 //Barra de progreso.
             });
  }

$("#btn_consultar").click(function () {
      $("#cuerpo_tabla").html('');
      subirArchivos();
});

$(function () {
    oFileIn = document.getElementById('archivo');
    if (oFileIn.addEventListener) {
      oFileIn.addEventListener('change', guardarEvento, false);
    }
});

function successFunction(data) {
var allRows = data.split(/\r?\n|\r/);
var valuehtml = '';
var num_ok = 0;
var num_err = 0;
var num_restartotal = 1;  // le resta un fila al archivo para no metter la ultima fila en blanco
for (var singleRow = 1; singleRow < (allRows.length - num_restartotal); singleRow++) {
              var rowCells = allRows[singleRow].split(';');
      if(rowCells){
              var formData = new FormData($('.formGuardar')[0]);

              formData.append("descripcion", rowCells)

                  $.ajax({
                      url: base_url + 'insert1',
                      type: 'POST',
                      data: formData,
                      cache: false,
                      contentType: false,
                      processData: false,
                      beforeSend: function () {
                      },
                      success: function (data) {

               if (data.indexOf("UNIQUE KEY constraint") > -1) {
                              alert("Existe un registro con este codigo");
                }else {
                               if (data == 1) {
                                          num_ok = num_ok + 1;
                                }
                                else {

                                    num_err = num_err + 1;
                                    var posicion_i = data.indexOf('[') + 1;
                                    var posicion_f = data.indexOf(']',posicion_i) - 1;
                                    var value_error = data.substring(posicion_i,posicion_f);

                                    var posicion_i = data.indexOf('[',posicion_f) + 1;
                                    var posicion_f = data.indexOf(']',posicion_i);
                                    var value_mensaje = data.substring(posicion_i,posicion_f);

                                    var posicion_i = data.indexOf('[',posicion_f) + 1;
                                    var posicion_f = data.indexOf(']',posicion_i);
                                    var value_data = data.substring(posicion_i,posicion_f);

                                    var messaja_data = value_error+" - "+value_mensaje;
                                    var array_data = value_data.split(',');

                                    valuehtml_aux = "<tr>";
                                    for (var i=0; i < array_data.length; i++) {
                                            valuehtml_aux = valuehtml_aux + '<td>'+array_data[i]+'</td>';
                                    }
                                    valuehtml_aux = valuehtml_aux + "<td>"+messaja_data+"</td><tr>";

                                    valuehtml = valuehtml + valuehtml_aux;

                                    $("#cuerpo_tabla").html(valuehtml);

                                  }

                                  $("#correctos").html(num_ok);
                                  $("#errores").html(num_err);
                                  var num_total = (num_ok + num_err);
                                  $("#total").html(num_total);

                                  let porcentaje2 = Math.floor((num_ok / (allRows.length - 1 - num_restartotal) * 100))
                                  $("#barprocesados").html(porcentaje2 + "%");
                                  $("#barprocesados").css("width", porcentaje2 + "%");

                                  let porcentaje3 = Math.floor((num_err / (allRows.length - 1 - num_restartotal) * 100))
                                  $("#barerror").html(porcentaje3 + "%");
                                  $("#barerror").css("width", porcentaje3 + "%");

                                  let porcentaje1 = Math.floor((num_total / (allRows.length - 1 - num_restartotal) * 100))
                                  $("#bartotal").html(porcentaje1 + "%");
                                  $("#bartotal").css("width", porcentaje1 + "%");



                                  let porcentaje = Math.floor((num_total / (allRows.length - 1 - num_restartotal) * 100))
                                  $("#barra").html(porcentaje + "%");
                                  $("#barra").css("width", porcentaje + "%");
                          }
                                },
                                //si ha ocurrido un error
                                error: function (FormData) {
                      console.log(data)
                                }

                            });

    }
}  //fin del for


}


$.fn.upload = function(remote, data, successFn, progressFn) {
// if we dont have post data, move it along
if (typeof data != "object") {
    progressFn = successFn;
    successFn = data;
}
return this.each(function() {
    if ($(this)[0].files[0]) {
        var formData = new FormData();
        formData.append($(this).attr("name"), $(this)[0].files[0]);

        // if we have post data too
        if (typeof data == "object") {
            for (var i in data) {
                formData.append(i, data[i]);
            }
        }
        // do the ajax request
        $.ajax({
            url: remote,
            type: 'POST',
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload && progressFn) {
                    myXhr.upload.addEventListener('progress', function(prog) {
                        var value = ~~((prog.loaded / prog.total) * 100);
                        // if we passed a progress function
                        if (progressFn && typeof progressFn == "function") {
                            progressFn(prog, value);
                            // if we passed a progress element
                        } else if (progressFn) {
                            $(progressFn).val(value);
                        }
                    }, false);
                }
                return myXhr;
            },
            data: formData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            complete: function(res) {
                var json;
                try {
                    json = JSON.parse(res.responseText);
                } catch (e) {
                    json = res.responseText;
                }
                if (successFn)
                    successFn(json);
            }
        });
    }
});
};
