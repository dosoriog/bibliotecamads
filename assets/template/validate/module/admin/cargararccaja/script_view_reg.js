function confirmar(){
      var cod_apertura = $('#aperturas').val();
      if(cod_apertura!=""){
                $('#aperturahead').val(cod_apertura);
                $( "#formsconfirmar" ).submit();
      }
      else {
          $("#modal-default").modal('show');
      }
}

function anular(){
      $( "#formsanular" ).submit();
}

function procesar(){
      $("#mi-modalprocesar").modal('hide');
      $("#mi-modalprocesando").modal('show');
      $( "#formsprocesar" ).submit();
}

function cancelar(){
      $( "#formListar" ).submit();
}

$(document).ready(function () {

        $('#listregistros').DataTable({
               "ordering": false,
               "language": {
                   "lengthMenu": "Mostrar _MENU_ registros por pagina",
                   "zeroRecords": "No se encontraron resultados en su busqueda",
                   "searchPlaceholder": "Buscar registros",
                   "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
                   "infoEmpty": "No existen registros",
                   "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                   "search": "Buscar:",
                   "paginate": {
                       "first": "Primero",
                       "last": "Último",
                       "next": "Siguiente",
                       "previous": "Anterior"
                   },
               }
           });

})
