$( "#FormDatReg" ).validate( {
  rules: {
    codigo: {
      required: true,
      minlength: 3,
      maxlength: 5
    },
    nombre: {
      required: true,
      minlength: 5
    },
  },
  messages: {
    codigo: {
      required: "Ingrese un codigo valido",
      minlength: "El codigo debe tener de 3 a 5.",
      maxlength: "Codigo de 3 a 5 caracteres."
    },
    nombre: {
      required: "Ingrese un nombre valido. Minimo 5 caractres",
      minlength: "Ingrese un nombre valido. Minimo 5 caractres"
    },
  },
  errorElement: "em",
  errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          error.addClass( "help-block" );

          // Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".form-group" ).addClass( "has-feedback" );

          if ( element.prop( "type" ) === "checkbox" ) {
            error.insertAfter( element.parent( "label" ) );
          } else {
            error.insertAfter( element );
          }

          // Add the span element, if doesn't exists, and apply the icon classes to it.
          if ( !element.next( "span" )[ 0 ] ) {
            $( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
          }
  },
  success: function ( label, element ) {
    // Add the span element, if doesn't exists, and apply the icon classes to it.
    if ( !$( element ).next( "span" )[ 0 ] ) {
      $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
    }
  },
  highlight: function ( element, errorClass, validClass ) {
        $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        $( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
  },
  unhighlight: function (element, errorClass, validClass) {
        $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        $( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
  }
} );
