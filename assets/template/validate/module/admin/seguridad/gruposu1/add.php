
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Registrar Rol/Grupo
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                             </div>
                        <?php endif;?>
                        <form action="<?php echo base_url();?>seguridad/gruposu/insert" method="POST">
                           <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="nombre">Nombre:</label>
                                   <input type="text" class="form-control text-uppercase" id="nombre" name="nombre">
                                  </div>
                              </div>
                              <div class="col-md-4">
                                     <div class="form-group">
                                          <label for="estado">Activo:</label>
                                          <label class="switch switch-text switch-success switch-pill">
                                          <input type="checkbox" class="switch-input" checked="true" name="estado" id="estado"> <span data-on="SI" data-off="NO" class="switch-label"></span> <span class="switch-handle"></span>
                                          </label>
                                    </div>
                              </div>
                          </div>
                            <div class="form-group">
                                <?php $this->_toolbaraddedit("Guardar","","Cancelar","seguridad/gruposu/");?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
