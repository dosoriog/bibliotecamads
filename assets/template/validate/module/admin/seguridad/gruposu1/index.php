
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Roles/Grupos usuarios
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <?php $this->_toolbarindex("Agregar Rol/Grupo usuario","seguridad/gruposu/add"); ?>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table id="listregistros" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($gruposu)):?>
                                    <?php foreach($gruposu as $grupou):?>
                                        <tr>
                                            <td><?php echo $grupou->gruscodigo;?></td>
                                            <td><?php echo $grupou->grusnombre;?></td>
                                            <td><?php echo $grupou->grusestado;?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <?php $this->_toolbarlist("seguridad/gruposu/edit","seguridad/gruposu/delete",$grupou,$grupou->gruscodigo); ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Informacion de la Categoria</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
