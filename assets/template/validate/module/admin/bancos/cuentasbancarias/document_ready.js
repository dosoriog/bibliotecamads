$(document).ready(function () {
  $(".btn-view").on("click", function(){
      var id = $(this).val();
      $.ajax({
          url: base_url + "view/" + id,
          type:"POST",
          success:function(resp){
              $("#modal-default .modal-body").html(resp);
          }

      });

  });

  $(".btn-delete").on("click", function(){
      var id = $(this).val();
      $.ajax({
          url: base_url + "viewelimina/" + id,
          type:"POST",
          success:function(resp){
              $("#modal-elimina .modal-body").html(resp);
          }

      });

  });

  $('#listregistros').DataTable({
         "columnDefs": [{ className: "text-right", targets: [ 5 ] }, { className: "text-center", targets: [ 0, 4, 6, 7 ] }],
         "ordering": false,
         "language": {
             "lengthMenu": "Mostrar _MENU_ registros por pagina",
             "zeroRecords": "No se encontraron resultados en su busqueda",
             "searchPlaceholder": "Buscar registros",
             "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
             "infoEmpty": "No existen registros",
             "infoFiltered": "(filtrado de un total de _MAX_ registros)",
             "search": "Buscar:",
             "paginate": {
                 "first": "Primero",
                 "last": "Último",
                 "next": "Siguiente",
                 "previous": "Anterior"
             },
         }
     });

})
