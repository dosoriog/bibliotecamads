	$( "#FormDatReg" ).validate( {
				rules: {
					documento: {
						required: true,
						minlength: 6
					},
					correo: {
						required: true,
						email: true
					}
				},
				messages: {
					documento: {
						required: "Ingrese numero del documento.",
						minlength: "El documento debe tener minimo 6 caracteres."
					},
					correo: "Ingrese un correo valido."
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-md-2" ).addClass( "has-feedback" );
          element.parents( ".col-md-6" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
			        $( element ).parents( ".col-md-2" ).addClass( "has-error" ).removeClass( "has-success" );
			        $( element ).parents( ".col-md-6" ).addClass( "has-error" ).removeClass( "has-success" );
			        $( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
			  },
			  unhighlight: function (element, errorClass, validClass) {
			        $( element ).parents( ".col-md-2" ).addClass( "has-success" ).removeClass( "has-error" );
			        $( element ).parents( ".col-md-6" ).addClass( "has-success" ).removeClass( "has-error" );
			        $( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
			  }
			} );
