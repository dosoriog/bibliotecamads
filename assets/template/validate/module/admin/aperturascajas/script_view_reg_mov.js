$('#fecha').datepicker({
  autoclose: true,
  todayBtn: true,
  showInputs: false,
  todayHighlight: true,
  format: format_fechas,
})
