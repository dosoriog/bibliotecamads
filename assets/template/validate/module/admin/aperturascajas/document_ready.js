$(document).ready(function () {
      $(".btn-cerrara").on("click", function(){
          var id = $(this).val();
          $('#id_aper').val(id);
          $.ajax({
              url: base_url + "viewcierraa/" + id,
              type:"POST",
              success:function(resp){
                  $("#modal-cerrara .modal-body").html(resp);
              }
            });
        });

      $(".btn-confirmacierre").on("click", function(){
          if($('#mvguardados').val()>0){
               alert("Para cerrar una apertura de caja no puede tener movimientos en estado GUARDADO. Debe CONFIRMAR o ELIMINAR los movimientos en estado GUARDADO actuales.");
          }else{
                  var id = $('#id_aper').val();
                  $.ajax({
                              type:'POST',
                              url:base_url+ "cerrarapertura/",
                              dataType: "json",
                              data:{id:id},
                              success:function(data){
                                  if(data.estado == 'ok'){
                                      location.reload();
                                  }else{
                                      alert("Error en la conexion...");
                                  }
                              }
                    });
              }
        });

        $(".btn-view").on("click", function(){
            var id = $(this).val();
            $.ajax({
                url: base_url + "view/" + id,
                type:"POST",
                success:function(resp){
                    $("#modal-default .modal-body").html(resp);
                }

            });

        });

        $(".btn-delete").on("click", function(){
            var id = $(this).val();
            $.ajax({
                url: base_url + "viewelimina/" + id,
                type:"POST",
                success:function(resp){
                    $("#modal-elimina .modal-body").html(resp);
                }

            });

        });

        $('#listregistros').DataTable({
               "ordering": false,
               "language": {
                   "lengthMenu": "Mostrar _MENU_ registros por pagina",
                   "zeroRecords": "No se encontraron resultados en su busqueda",
                   "searchPlaceholder": "Buscar registros",
                   "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
                   "infoEmpty": "No existen registros",
                   "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                   "search": "Buscar:",
                   "paginate": {
                       "first": "Primero",
                       "last": "Último",
                       "next": "Siguiente",
                       "previous": "Anterior"
                   },
               }
           });
    

})
