$(document).ready(function () {

  $('#listmovimientoscaja').DataTable({
         "ordering": false,
         "language": {
             "lengthMenu": "Mostrar _MENU_ registros por pagina",
             "zeroRecords": "No se encontraron resultados en su busqueda",
             "searchPlaceholder": "Buscar registros",
             "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
             "infoEmpty": "No existen registros",
             "infoFiltered": "(filtrado de un total de _MAX_ registros)",
             "search": "Buscar:",
             "paginate": {
                 "first": "Primero",
                 "last": "Último",
                 "next": "Siguiente",
                 "previous": "Anterior"
             },
         }
     });

     $(".btn-seleccionamov").on("click", function(){
         var value = $(this).val();
         var valArray = value.split('|');
         $('#idmov').val(valArray[0]);
         $('#conceptos').val(valArray[1]).trigger('change.select2');
         $('#requieret').val(valArray[2]);
         if(valArray[2]=='SI'){
              $('#tercero').val(valArray[3]).trigger('change.select2');
           }
         else{
              $('#tercero').val(0).trigger('change.select2');
         }
         $('#fecha').val(valArray[4]);
         $('#valor').val(valArray[5]);
         $('#detalle').val(valArray[6]);
         $('#estado').val(valArray[7]);

         $('#ugrabado').val(valArray[8]+', '+valArray[10]);

         $('#uconfirmado').val(valArray[11]+', '+valArray[12]+', '+valArray[13]);

         $('#uanulado').val(valArray[14]+', '+valArray[15]+', '+valArray[16]);

         $('#estmov').val(valArray[17]);

         $('#movtdoc').val(valArray[18]);
         $('#movtpdc').val(valArray[19]);
         $('#movdocu').val(valArray[20]);
         $('#movnume').val(valArray[21]);
         $('#num_documento').val(valArray[21]);
         $('#movidreg').val(valArray[22]);


         if($('#estadoapertura').val()=="ACTIVO"){
                 $('#btnguarda').prop('disabled', true);
                 $('#btnupload').prop('disabled', true);
                 if(valArray[17]==3){
                     $('#btnconfirma').prop('disabled', false);
                     $('#btnelimina').prop('disabled', false);
                 }
                 else {
                    $('#btnconfirma').prop('disabled', true);
                    $('#btnelimina').prop('disabled', true);
                 }
                 if(valArray[17]==6){
                     $('#btnanula').prop('disabled', false);
                 }
                 else {
                     $('#btnanula').prop('disabled', true);
                 }
           }
         $('#btncancelar').prop('disabled', false);

         $('#conceptos').prop('disabled', true);
         $('#tercero').prop('disabled', true);
         $('#fecha').prop('disabled', true);
         $('#valor').prop('disabled', true);
         $('#detalle').prop('disabled', true);

     });

     $(".btn-upload").on("click", function(){
         $("#modal-uploasdoc").modal('show');
     });

     $(".btn-cargardocumento").on("click", function(){
                   var id = $('#documento').val();
                   var swedit = $('#swedit').val();
                   if((!id)||(id=='')){
                             alert('Debe seleccionar un documento pendiente por egreso o pago.');
                             $('#documento').focus();
                   }else{
                           $.ajax({
                                   type:'POST',
                                   url:base_url+ "buscardoc",
                                   dataType: "json",
                                   data:{id:id},
                                   success:function(data){
                                       if(data.estado == 'ok'){
                                         if($('#tipocaja').val()=="CAJ"){
                                             $('#conceptos').val(data.registro.codconcepto).trigger('change.select2');
                                         }else {
                                               if($('#tipocaja').val()=="CTA"){
                                                   $('#conceptos').val(data.registro.codconcepto_nb).trigger('change.select2');
                                               }
                                         }
                                         $('#fecha').val(data.registro.fecha);
                                         $('#requieret').val(data.registro.req_tercero);
                                         $('#tercero').val(data.registro.idpersona).trigger('change.select2');
                                         $('#valor').val(data.registro.valor);

                                         $('#sw_mov').val("2");
                                         $('#iddoc').val(data.registro.id);
                                         $('#tdoc').val(data.registro.codigo);
                                         $('#doc').val(data.registro.documento);
                                         $('#num').val(data.registro.numero);
                                         if($('#tipocaja').val()=="CAJ"){
                                             $('#ccon').val(data.registro.codconcepto);
                                         }else {
                                               if($('#tipocaja').val()=="CTA"){
                                                   $('#ccon').val(data.registro.codconcepto_nb);
                                               }
                                         }
                                         $('#tcon').val(data.registro.mov_tipo);
                                         $('#rter').val(data.registro.req_tercero);
                                         $('#fec').val(data.registro.fecha);
                                         $('#val').val(data.registro.valor);
                                         $('#tidp').val(data.registro.idpersona);
                                         $('#tide').val(data.registro.tipodoc);
                                         $('#tper').val(data.registro.codtipo);
                                         $('#ide').val(data.registro.identificacion);
                                         $('#nom').val(data.registro.persona);
                                         $('#toter').val(data.registro.tieneope_ter);


                                         $('#btncancelar').prop('disabled', false);

                                         $('#conceptos').prop('disabled', true);
                                         $('#tercero').prop('disabled', true);
                                         $('#fecha').prop('disabled', true);
                                         $('#valor').prop('disabled', true);
                                         $('#detalle').prop('disabled', false);

                                         $("#modal-uploasdoc").modal('hide');
                                       }else{
                                           alert("Error en la conexion...");
                                           $("#modal-uploasdoc").modal('hide');
                                       }
                                   }
                               });
                   }
     });

     $(".btn-cancelarmov").on("click", function(){
         $('#idmov').val('');
         $('#requieret').val('');
         $('#tercero').val(0).trigger('change.select2');
         $('#conceptos').val(0).trigger('change.select2');

         $('#fecha').val('');
         $('#valor').val('');
         $('#detalle').val('');

         $('#ugrabado').val('');
         $('#uconfirmado').val('');
         $('#uanulado').val('');
         $('#estado').val('');
         $('#estmov').val('');
         $('#sw_mov').val("1");
         if($('#estadoapertura').val()=="ACTIVO"){
                 $('#btnguarda').prop('disabled', false);
                 $('#btnupload').prop('disabled', false);
                 $('#btnconfirma').prop('disabled', true);
                 $('#btnanula').prop('disabled', true);
                 $('#btnelimina').prop('disabled', true);
           }
         $('#btncancelar').prop('disabled', true);

         $('#conceptos').prop('disabled', false);
         $('#tercero').prop('disabled', false);
         $('#fecha').prop('disabled', false);
         $('#valor').prop('disabled', false);
         $('#detalle').prop('disabled', false);

     });

     var modalConfirm = function(callback){

               $("#btnconfirma").on("click", function(){
                 $("#mi-modal").modal('show');
               });

               $("#modal-btn-si").on("click", function(){
                 callback(true);
                 $("#mi-modal").modal('hide');
               });

               $("#modal-btn-no").on("click", function(){
                 callback(false);
                 $("#mi-modal").modal('hide');
               });
     };

     modalConfirm(function(confirm){
                 if(confirm){
                   var id    = $('#idmov').val();
                   var ap    = $('#aperturacaja').val();
                   var cj    = $('#caja').val();
                   var tcj    = $('#tipocaja').val();
                   var mvcon = $('#mvcon').val();
                   var mvgua = $('#mvgua').val();
                   var cp    = $('#conceptos').val();
                   var vl    = $('#valor').val();
                   var est   = $('#estmov').val();
                   var nest  = $('#estado').val();
                   var mvidreg = $('#movidreg').val();
                   var movtpdc = $('#movtpdc').val();
                   if((!id)||(id=='')){
                             alert('Debe seleccionar un movimiento en estado <GRABADO> pendiente por <CONFIRMADO>.');
                   }else {
                              if((!est)||(est=='')||(est!='3')){
                                   alert('Debe seleccionar un movimiento en estado <GRABADO>. El actual se encuentra en estado <'+nest+'>');
                             }else{
                                         $.ajax({
                                                     type:'POST',
                                                     url:base_url+ "confirmar/",
                                                     dataType: "json",
                                                     data:{id:id,ap:ap,cj:cj,tcj:tcj,cp:cp,vl:vl,mvcon:mvcon,mvgua:mvgua,mvidreg:mvidreg,movtpdc:movtpdc},
                                                     success:function(data){
                                                         if(data.estado == 'ok'){
                                                             location.reload();

                                                             $('#idmov').val(data.registro.id);
                                                             $('#requieret').val(data.registro.conceprequieretercero);
                                                             if(data.registro.conceprequieretercero=='SI'){
                                                                  $('#tercero').val(data.registro.movitercero).trigger('change.select2');
                                                               }
                                                             else{
                                                                  $('#tercero').val(0).trigger('change.select2');
                                                             }
                                                             $('#identificacion').val(data.registro.tipodoc+' - '+data.registro.identificacion);
                                                             $('#persona').val(data.registro.persona);
                                                             $('#docm').val(data.registro.codigo+' - '+data.registro.documento);
                                                           //  $('#numero').val(data.registro.numero);
                                                             $('#valor').val(data.registro.movivalor);
                                                             $('#fecha').val(data.registro.fecha);
                                                             $('#estado').val(data.registro.estado);
                                                             $('#estmov').val(data.registro.moviestado);

                                                             $('#iddoc').val(data.registro.id);
                                                             $('#tdoc').val(data.registro.codigo);
                                                             $('#doc').val(data.registro.documento);
                                                             $('#num').val(data.registro.numero);
                                                             $('#fec').val(data.registro.fecha);
                                                             $('#val').val(data.registro.valor);
                                                             $('#tide').val(data.registro.tipodoc);
                                                             $('#ide').val(data.registro.identificacion);
                                                             $('#nom').val(data.registro.persona);

                                                             $('#ugrabado').val(data.registro.usuarioreg+', '+data.registro.horaeli);

                                                             $('#uconfirmado').val(data.registro.usuarioconfirma+', '+data.registro.horaconfirma);

                                                             $('#uanulado').val(data.registro.usuarioanula+', '+data.registro.horaanula);




                                                         }else{
                                                             //$('.user-content').slideUp();
                                                             alert("Error en la conexion...");
                                                         }
                                                     }
                                                 });
                                   }
                         }
                 }
       });

       var modalConfirma = function(callback){

                 $("#btnanula").on("click", function(){
                       var mtd = $('#movtdoc').val();
                       if(mtd=='SI'){
                             var mtipd = $('#movtpdc').val();
                             var mdocu = $('#movdocu').val();
                             var mnume = $('#movnume').val();
                             $("#modal-msj .modal-body").html('<p>No se puede realizar la operacion de anulacion en un movimiento con documentos asociados.</p><p><strong>Tipo Documento:</strong> '+ mtipd +' - '+ mdocu +'</p><p><strong>Numero Documento:</strong> '+mnume+'</p>');
                             $("#modal-msj").modal('show');
                       }else{
                             $("#mi-modalanula").modal('show');
                         }
                 });

                 $("#modala-btn-si").on("click", function(){
                   callback(true);
                   $("#mi-modalanula").modal('hide');
                 });

                 $("#modala-btn-no").on("click", function(){
                   callback(false);
                   $("#mi-modalanula").modal('hide');
                 });
       };

       modalConfirma(function(confirm){
               if(confirm){
                 var id    = $('#idmov').val();
                 var ap    = $('#aperturacaja').val();
                 var mvanu = $('#mvanu').val();
                 var mvcon = $('#mvcon').val();
                 var cj    = $('#caja').val();
                 var tcj   = $('#tipocaja').val();
                 var cp    = $('#conceptos').val();
                 var vl    = $('#valor').val();
                 var est   = $('#estmov').val();
                 var nest  = $('#estado').val();
                 var mvidreg = $('#movidreg').val();
                 var movtpdc = $('#movtpdc').val();

                 if((!id)||(id=='')){
                           alert('Debe seleccionar un movimiento en estado <CONFIRMADO>.');
                 }else {
                               if((!est)||(est=='')||(est!='6')){
                                     alert('Debe seleccionar un movimiento en estado <CONFIRMADO>. El actual se encuentra en estado <'+nest+'>');
                               }else{
                                           $.ajax({
                                                       type:'POST',
                                                       url:base_url+ "anular/",
                                                       dataType: "json",
                                                       data:{id:id,ap:ap,cj:cj,tcj:tcj,cp:cp,vl:vl,mvanu:mvanu,mvcon:mvcon,mvidreg:mvidreg,movtpdc:movtpdc},
                                                       success:function(data){
                                                           if(data.estado == 'ok'){
                                                               location.reload();

                                                               $('#idmov').val(data.registro.id);
                                                               $('#requieret').val(data.registro.conceprequieretercero);
                                                               if(data.registro.conceprequieretercero=='SI'){
                                                                    $('#tercero').val(data.registro.movitercero).trigger('change.select2');
                                                                 }
                                                               else{
                                                                    $('#tercero').val(0).trigger('change.select2');
                                                               }
                                                               $('#identificacion').val(data.registro.tipodoc+' - '+data.registro.identificacion);
                                                               $('#persona').val(data.registro.persona);
                                                               $('#docm').val(data.registro.codigo+' - '+data.registro.documento);
                                                             //  $('#numero').val(data.registro.numero);
                                                               $('#valor').val(data.registro.movivalor);
                                                               $('#fecha').val(data.registro.fecha);
                                                               $('#estado').val(data.registro.estado);
                                                               $('#estmov').val(data.registro.moviestado);

                                                               $('#iddoc').val(data.registro.id);
                                                               $('#tdoc').val(data.registro.codigo);
                                                               $('#doc').val(data.registro.documento);
                                                               $('#num').val(data.registro.numero);
                                                               $('#fec').val(data.registro.fecha);
                                                               $('#val').val(data.registro.valor);
                                                               $('#tide').val(data.registro.tipodoc);
                                                               $('#ide').val(data.registro.identificacion);
                                                               $('#nom').val(data.registro.persona);

                                                               $('#ugrabado').val(data.registro.usuarioreg+', '+data.registro.horaeli);

                                                               $('#uconfirmado').val(data.registro.usuarioconfirma+', '+data.registro.horaconfirma);

                                                               $('#uanulado').val(data.registro.usuarioanula+', '+data.registro.horaanula);




                                                           }else{
                                                               //$('.user-content').slideUp();
                                                               alert("Error en la conexion...");
                                                           }
                                                       }
                                                   });
                                     }

                       }
               }
             });

             var modalElimina = function(callback){
                       $("#btnelimina").on("click", function(){
                         var mtd = $('#movtdoc').val();
                         if(mtd=='SI'){
                               var mtipd = $('#movtpdc').val();
                               var mdocu = $('#movdocu').val();
                               var mnume = $('#movnume').val();
                               $("#modal-msj .modal-body").html('<p>No se puede realizar la operacion de eliminacion en un movimiento con documentos asociados.</p><p><strong>Tipo Documento:</strong> '+ mtipd +' - '+ mdocu +'</p><p><strong>Numero Documento:</strong> '+mnume+'</p>');
                               $("#modal-msj").modal('show');
                         }else{
                               $("#mi-modalelimina").modal('show');
                           }
                       });

                       $("#modale-btn-si").on("click", function(){
                         callback(true);
                         $("#mi-modalelimina").modal('hide');
                       });

                       $("#modale-btn-no").on("click", function(){
                         callback(false);
                         $("#mi-modalelimina").modal('hide');
                       });
             };

             modalElimina(function(confirm){
                     if(confirm){
                       var id   = $('#idmov').val();
                       var est  = $('#estmov').val();
                       var nest = $('#estado').val();
                       var mveli = $('#mveli').val();
                       var mvgua = $('#mvgua').val();
                       var mvidreg = $('#movidreg').val();
                       var movtpdc = $('#movtpdc').val();
                       var vl    = $('#valor').val();
                       var ap    = $('#aperturacaja').val();
                       if((!id)||(id=='')){
                                 alert('Debe seleccionar un movimiento en estado <GRABADO>.');
                       }else {
                                  if((!est)||(est=='')||(est!='3')){
                                       alert('Debe seleccionar un movimiento en estado <GRABADO>. El actual se encuentra en estado <'+nest+'>');
                                 }else{
                                             $.ajax({
                                                         type:'POST',
                                                         url:base_url+ "eliminarm/",
                                                         dataType: "json",
                                                         data:{id:id,ap:ap,mveli:mveli,mvgua:mvgua,mvidreg:mvidreg,movtpdc:movtpdc,vl:vl},
                                                         success:function(data){
                                                             if(data.estado == 'ok'){
                                                                 location.reload();
                                                             }else{
                                                                 //$('.user-content').slideUp();
                                                                 alert("Error en la conexion...");
                                                             }
                                                         }
                                                     });
                                       }
                             }
                     }
                   });

     $(".btn-view").on("click", function(){
         var id = $(this).val();
         $.ajax({
             url: base_url + "viewdetallemov/" + id,
             type:"POST",
             success:function(resp){
                 $("#modal-default .modal-body").html(resp);
                 //alert(resp);
             }

         });

     });
     $(".btn-tercerodoc").on("click", function(){
         var id = $(this).val();
         $.ajax({
             url: base_url + "viewbusqueda/",
             type:"POST",
             success:function(resp){
                 $("#modal-tercerodoc .modal-body").html(resp);

                 $('#listregistrosb').DataTable({
                        "language": {
                            "lengthMenu": "Mostrar _MENU_ registros por pagina",
                            "zeroRecords": "No se encontraron resultados en su busqueda",
                            "searchPlaceholder": "Buscar registros",
                            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
                            "infoEmpty": "No existen registros",
                            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "search": "Buscar:",
                            "paginate": {
                                "first": "Primero",
                                "last": "Último",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                        }
                    });

               //  alert(resp);
             }

         });

     });

     $(".btn-view1").on("click", function(){
                 var id = $(this).val();
                 alert("Esta accion se ejecuta al cerrar el modal")
     });

})
