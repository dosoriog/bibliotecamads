$('#fecha').datepicker({
  autoclose: true,
  todayBtn: true,
  showInputs: false,
  todayHighlight: true,
  format: format_fechas,
});

$(".btn-gfcompra").on("click", function(){
    var id = $(this).val();
    var numero = $('#numero').val();
    var fecha = $('#fecha').val();
    var descuento = $('#descuento').val();
    var impuesto = $('#impuesto').val();
    var numreg = $('#numreg').val();
    var swedit1 = $('#swedit1').val();
    var descripcion = $('#descripcion').val();
    if(!descuento){ descuento = 0; }
    if(!impuesto){ impuesto = 0; }
    var proveedor = $('#persona').val();
    if((!numero)||(numero==0)){
              $('#modal-default .modal-body').html('<h4>Ingrese numero de la cotizacion.</h4>');
              $('#numero').focus();
    }else{

       if((!proveedor)||(proveedor==0)){
                 $("#modal-default .modal-body").html('<h4>Seleccione el tercero de la cotizacion.</h4>');
                 $('#persona').focus();
          }else{

            if((!numreg)||(numreg==0)){
                      $("#modal-default .modal-body").html('<h4>Debe agregar el detalle de la cotizacion.</h4>');
                      $('#persona').focus();
               }else{
                          $("#modal-default .modal-body").html('<h4>Guardando cotizacion...</h4>');
                          $.ajax({
                              url: base_url + "insert",
                              data: {id:id,numero:numero, fecha:fecha, proveedor:proveedor,descuento:descuento, impuesto:impuesto, descripcion:descripcion, swedit1:swedit1},
                              type:"POST",
                              success:function(resp){
                                   self.location.href = resp;
                              }

                          });
                }

            }
      }

});

var modalConfirm = function(callback){

          $("#btnconfirma").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });

          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
};

modalConfirm(function(confirm){
            if(confirm){
              var id      = $('#id').val();
              var valdec = $('#descuento').val();
              if((!valdec)||(valdec=='')){ valdec = 0; }

              var valimp = $('#impuesto').val();
              if((!valimp)||(valimp=='')){ valimp = 0; }

              $('#val_dec').val(valdec);
              $('#val_imp').val(valimp);
              var est     = $('#estfactc').val();
              if((!id)||(id=='')){
                        alert('Debe seleccionar una cotizacion en estado <GRABADO> pendiente por <CONFIRMAR>.');
              }else {
                         if((!est)||(est=='')||(est!='3')){
                              alert('Estado no valido. Debe seleccionar una cotizacion en estado <GRABADO>.');
                        }else{
                                      $( "#frmconfirmafactc" ).submit();
                              }
                    }
            }
  });
  var modalElimina = function(callback){
            $("#btnelimina").on("click", function(){
                $("#mi-modalelimina").modal('show');
            });
            $("#modale-btn-si").on("click", function(){
              callback(true);
              $("#mi-modalelimina").modal('hide');
            });

            $("#modale-btn-no").on("click", function(){
              callback(false);
              $("#mi-modalelimina").modal('hide');
            });
  };

  modalElimina(function(confirm){
          if(confirm){
                var id      = $('#id').val();
                var est     = $('#estfactc').val();
                if((!id)||(id=='')){
                          alert('Debe seleccionar una cotizacion en estado <GRABADO>.');
                }else {
                           if((!est)||(est=='')||(est!='3')){
                                alert('Estado no valido. Debe seleccionar una cotizacion en estado <GRABADO>.');
                          }else{
                                        $( "#frmeliminafactc" ).submit();
                                }
                      }
          }
        });

$(".btn-gfcancelar").on("click", function(){
    var id = $(this).val();
    var numero = $('#numero').val();
    var fecha = $('#fecha').val();
    var descuento = $('#descuento').val();
    var impuesto = $('#impuesto').val();
    var numreg = $('#numreg').val();
    var swedit1 = $('#swedit1').val();
    if(!descuento){ descuento = 0; }
    if(!impuesto){ impuesto = 0; }
    var proveedor = $('#persona').val();
    $.ajax({
        url: base_url + "cancelarfactura",
        data: {id:id,numero:numero, fecha:fecha, proveedor:proveedor,descuento:descuento, impuesto:impuesto, swedit1:swedit1},
        type:"POST",
        success:function(resp){
             self.location.href = resp;
        }

    });

});

function btn_regresarindex(){
      sw_g = $('#sw_guardar').val();
      if(sw_g=="SI"){
             $("#confi-guardar").modal('show');
      }else{
             self.location.href = base_url;
        }
}

function btn_imprimir(id){
        $.ajax({
            url: base_url + "imprimir",
            data: {id:id},
            type:"POST",
            success:function(resp){
                  newWin = window.open("");
                  newWin.document.write(resp);
                  //newWin.print();
                  //newWin.close();
            }

        });
}
