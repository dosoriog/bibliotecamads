
$("#codbarra").keypress(function(e) {
  //if (tecla==43) { pulsaracceso(e); return false; }
//  if (tecla==45) { pulsaracceso(e); return false; }
  if(e.which == 13) {
    $("#codbar_add").val($("#codbarra").val());
    $("#sw_busqueda").val("1");
    $("#codbarra").val("");
    $.ajax({
            type: $("#frmadd_item").attr("method"),
            url: $("#frmadd_item").attr("action"),
            data:$("#frmadd_item").serialize(),
            beforeSend: function(){  /* Esta función se ejecuta durante el envió de la petición al * servidor.  * */
            },
            complete:function(data){    /*    * Se ejecuta al termino de la petición    * */
            },
            success: function(data){  /*  * Se ejecuta cuando termina la petición y esta ha sido   * correcta * */
                actualizarTabla(data);
            },
            error: function(data){
                /*
                * Se ejecuta si la peticón ha sido erronea
                * */
                alert("Problemas al tratar de enviar el formulario");
                alert(data);
            }
        });

  }
});

function GuadarFctventa(){
  $("#codbarra").val("");
  $("#cliente_grb").val($("#persona").val());
  $("#valor_efectivo_grb").val($("#efectivo").val());
  $("#valor_tcredito_grb").val($("#tcredito").val());
  $("#valor_tdebito_grb").val($("#tdebito").val());
//  $("#Guardafrm_vtn").submit();
  $.ajax({
          type: $("#Guardafrm_vtn").attr("method"),
          url: $("#Guardafrm_vtn").attr("action"),
          data:$("#Guardafrm_vtn").serialize(),
          beforeSend: function(){
          },
          complete:function(data){
          },
          success: function(data){
            // alert(data);
             $("#modal-imprimirfact .modal-body").html(data);
             $("#modal-imprimirfact").modal('show');
             limpiarfact();
          },
          error: function(data){
            //alert(data);
              alert("Problemas al tratar de guardar el pago.");
          }
      });
}
function cobrarfact(){
   var counter = $("#num_item_tabla").val();
   if(counter > 0){
        $("#modal-default").modal('show');
    }
}
function modificarItem(idItem,nomItem,cantItem){
     $("#num_item_tabla").val(idItem);
     $("#itemselect").val(nomItem);
     $("#cantidad_aux").val(cantItem);
     $("#modal-selectitem").modal('show');
}
function limpiarfact(){
  $.ajax({
          type: $("#frm_limpiarfactura").attr("method"),
          url: $("#frm_limpiarfactura").attr("action"),
          data:$("#frm_limpiarfactura").serialize(),
          beforeSend: function(){
          },
          complete:function(data){
          },
          success: function(data){
                  //alert(data);
                  limpiartabla();
                  $("#id_temp_grb").val(data);
                  $("#id_temp_add").val(data);
                  $("#id_temp_limp").val(data);

                  $("#num_item_tabla").val("0");
                  $("#subtotal").val("0");
                  $("#descuento").val("0");
                  $("#impuesto").val("0");
                  $("#total").val("0");
                  $("#tpagar").html("0");
                  $("#tpagarletras").html("Cero");

                  $("#valor_efectivo_grb").val("0");
                  $("#valor_tcredito_grb").val("0");
                  $("#valor_tdebito_grb").val("0");

          },
          error: function(data){
              alert("Problemas al tratar de enviar el formulario");
              alert(data);
          }
      });
}
function limpiartabla(){
  var t = $('#listregistros').DataTable();
  var counter = $("#num_item_tabla").val();
  for(var i = 0;i < counter; i++){
      t.row(':eq(0)', { page: 'current' }).remove().draw();
   }
}
function actualizarTabla(valData){
          var valNew  = valData.split('!');
          var valDet  = valNew[1].split('¬');
          var t = $('#listregistros').DataTable();
          var counter = $("#num_item_tabla").val();

          limpiartabla();

          for(var numitem = 0;numitem < valDet.length; numitem++){
                      var valReg  = valDet[numitem].split('|');
                      var item = numitem + 1;
                      t.row.add( [
                          item,
                          valReg[1],
                          "$ "+number_format(valReg[2], 0),
                          valReg[3],
                          valReg[4]+'% - $'+number_format(valReg[5], 0),
                          valReg[6]+'% - $'+number_format(valReg[7], 0),
                          "$ "+number_format(valReg[8], 0),
                          "<button type=\"button\" class=\"btn btn-info btn-view  "+btn_tamano+" \"  value=\"0\" onclick=\"modificarItem('','"+valNew[1]+"','"+valNew[3]+"');\"><span class=\"fa fa-eye\"></span></button>"
                      ] ).draw( false );
            }
          $("#num_item_tabla").val(numitem);
          $("#subtotal").val("$ "+number_format(valNew[2], 0));
          $("#descuento").val("$ "+number_format(valNew[3], 0));
          $("#impuesto").val("$ "+number_format(valNew[4], 0));
          $("#total").val("$ "+number_format(valNew[5], 0));
          $("#total_venta").val(valNew[5]);
          $("#tpagar").html("$ "+number_format(valNew[5], 0));

          $("#tpagarletras").html(converNumLetras(valNew[5]));
}
function additem_clic(id){
  $("#codbar_add").val(id);
  $("#sw_busqueda").val("2");
  $("#codbarra").val("");
  $.ajax({
          type: $("#frmadd_item").attr("method"),
          url: $("#frmadd_item").attr("action"),
          data:$("#frmadd_item").serialize(),
          beforeSend: function(){
          },
          complete:function(data){
          },
          success: function(data){
            actualizarTabla(data);
          },
          error: function(data){
              alert("Problemas al tratar de enviar el formulario");
              alert(data);
          }
      });
}

 $('#listregistros').DataTable({
   "columnDefs": [{ className: "text-right", targets: [ 2, 4, 5, 6 ] }],
        "ordering": false,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
