$(".btn-cargardocumento").on("click", function(){
              var id = $('#documento').val();
              var swedit = $('#swedit').val();
              if((!id)||(id=='')){
                        alert('Debe seleccionar un documento pendiente por CXC.');
                        $('#documento').focus();
              }else{
                      $.ajax({
                              type:'POST',
                              url:base_url+ "buscardoc",
                              dataType: "json",
                              data:{id:id},
                              success:function(data){
                                  if(data.estado == 'ok'){
                                      $('#identificacion').val(data.registro.tipodoc+' - '+data.registro.identificacion);
                                      $('#persona').val(data.registro.persona);
                                      $('#docm').val(data.registro.codigo+' - '+data.registro.documento);
                                      $('#numero').val(data.registro.numero);
                                      $('#valor').val(data.registro.total);
                                      $('#fecha').val(data.registro.fecha);
                                      $('#iddoc').val(data.registro.id);
                                      $('#tdoc').val(data.registro.codigo);
                                      $('#doc').val(data.registro.documento);
                                      $('#num').val(data.registro.numero);
                                      $('#ccon').val(data.registro.codconcepto);
                                      $('#fec').val(data.registro.fecha);
                                      $('#val').val(data.registro.valor);
                                      $('#tidp').val(data.registro.idpersona);
                                      $('#tide').val(data.registro.tipodoc);
                                      $('#tper').val(data.registro.codtipo);
                                      $('#ide').val(data.registro.identificacion);
                                      $('#nom').val(data.registro.persona);

                                      $('#docm').prop('disabled', true);
                                      $('#numero').prop('disabled', true);
                                      $('#identificacion').prop('disabled', true);
                                      $('#persona').prop('disabled', true);
                                      $('#valor').prop('disabled', true);
                                      $('#fecha').prop('disabled', true);

                                  }else{
                                      alert("Error en la conexion...");
                                  }
                              }
                          });
              }
});

var modalConfirm = function(callback){

          $("#btnconfirma").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });

          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
};

modalConfirm(function(confirm){
            if(confirm){
              var id      = $('#id').val();

              var est     = $('#estadoreg').val();
              if((!id)||(id=='')){
                        alert('Debe seleccionar una CXC en estado <GRABADO> pendiente por <CONFIRMAR>.');
              }else {
                         if((!est)||(est=='')||(est!='3')){
                              alert('Estado no valido. Debe seleccionar una CXC en estado <GRABADO>.');
                        }else{
                                      $( "#frmconfirma" ).submit();
                              }
                    }
            }
  });
  var modalElimina = function(callback){
            $("#btnelimina").on("click", function(){
                $("#mi-modalelimina").modal('show');
            });
            $("#modale-btn-si").on("click", function(){
              callback(true);
              $("#mi-modalelimina").modal('hide');
            });

            $("#modale-btn-no").on("click", function(){
              callback(false);
              $("#mi-modalelimina").modal('hide');
            });
  };

  modalElimina(function(confirm){
          if(confirm){
                var id      = $('#id').val();
                var est     = $('#estadoreg').val();
                if((!id)||(id=='')){
                          alert('Debe seleccionar una CXC en estado <GRABADO>.');
                }else {
                           if((!est)||(est=='')||(est!='3')){
                                alert('Estado no valido. Debe seleccionar una CXC en estado <GRABADO>.');
                          }else{
                                        $( "#frmeliminacxp" ).submit();
                                }
                      }
          }
        });

$(".btn-gfcancelar").on("click", function(){
    var id = $(this).val();
    var numero = $('#numero').val();
    var fecha = $('#fecha').val();
    var descuento = $('#descuento').val();
    var impuesto = $('#impuesto').val();
    var numreg = $('#numreg').val();
    var swedit1 = $('#swedit1').val();
    if(!descuento){ descuento = 0; }
    if(!impuesto){ impuesto = 0; }
    var proveedor = $('#persona').val();
    $.ajax({
        url: base_url + "cancelarfactura",
        data: {id:id,numero:numero, fecha:fecha, proveedor:proveedor,descuento:descuento, impuesto:impuesto, swedit1:swedit1},
        type:"POST",
        success:function(resp){
             self.location.href = resp;
        }

    });

});

$("#modale-btn-addabono").on("click", function(){
        $('#modale-btn-addabono').prop('disabled', true);
        var id        = $('#id').val();
        var est       = $('#estadoreg').val();
        var apert     = $('#aperturaabono').val();
        var valorabo  = $('#valorabono').val();
        if((!id)||(id=='')){
                  alert('Debe seleccionar una CXC en estado <GRABADO> pendiente por <CONFIRMAR>.');
                  $('#modale-btn-addabono').prop('disabled', false);
        }else {
                   if((!est)||(est=='')||(est!='6')){
                        alert('Estado no valido. Debe seleccionar una CXC en estado <GRABADO>.');
                        $('#modale-btn-addabono').prop('disabled', false);
                  }else{
                              if((!apert)||(apert=='')||(apert=='0')){
                                        alert('Debe seleccionar una apertura de caja para registrar el abono.');
                                        $('#modale-btn-addabono').prop('disabled', false);
                              }else {
                                        if((!valorabo)||(valorabo=='')||(valorabo=='0')){
                                                  alert('Dijite el valor para registrar el abono.');
                                                  $('#modale-btn-addabono').prop('disabled', false);
                                        }else {
                                             $('#apert_abo').val(apert);
                                             $('#valor_abo').val(valorabo);
                                             $( "#frmregabono" ).submit();
                                        }
                                }
                        }
              }
});

$("#modale-btn-sianula").on("click", function(){
        $('#modale-btn-sianula').prop('disabled', true);
        var id        = $('#id_anula').val();
        var est       = $('#estadoreg').val();
        if((!id)||(id=='')){
                  alert('Debe seleccionar una CXC en estado <CONFIRMADO>.');
                  $('#modale-btn-sianula').prop('disabled', false);
        }else {
                   if((!est)||(est=='')||(est!='6')){
                        alert('Estado no valido. Debe seleccionar una CXC en estado <CONFIRMADO>.');
                        $('#modale-btn-sianula').prop('disabled', false);
                  }else{
                            $( "#frmanular" ).submit();
                        }
              }
});

$("#modale-btn-noabono").on("click", function(){
  $("#mi-modalabono").modal('hide');
});

function btn_agregarabono(){
      $("#mi-modalabono").modal('show');
}

function btn_anularcxp(){
      $("#mi-modalanula").modal('show');
}

function btn_cancelarabono(){
      $("#mi-modalabono").modal('hide');
}

function btn_cancelaranula(){
      $("#mi-modalanula").modal('hide');
}

function btn_regresarindex(){
      sw_g = $('#sw_guardar').val();
      if(sw_g=="SI"){
             $("#confi-guardar").modal('show');
      }else{
             self.location.href = base_url;
        }
}

function btn_anularcxp(){
      $("#mi-modalanula").modal('show');
}

function btn_imprimir(id){
        $.ajax({
            url: base_url + "imprimir",
            data: {id:id},
            type:"POST",
            success:function(resp){
                  newWin = window.open("");
                  newWin.document.write(resp);
            }

        });
}
