$(document).ready(function () {
  aplica_formatnum('valor');
  aplica_formatnum('abonos');
  aplica_formatnum('saldo');
  aplica_formatnum('valorabono');

  $('#listregistros').DataTable({
         "columnDefs": [{ className: "text-right", targets: [ 4 ] },{ className: "text-center", targets: [ 0, 2, 3, 5 ] }],
         "ordering": false,
         "language": {
             "lengthMenu": "Mostrar _MENU_ registros por pagina",
             "zeroRecords": "No se encontraron resultados en su busqueda",
             "searchPlaceholder": "Buscar registros",
             "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
             "infoEmpty": "No existen registros",
             "infoFiltered": "(filtrado de un total de _MAX_ registros)",
             "search": "Buscar:",
             "paginate": {
                 "first": "Primero",
                 "last": "Último",
                 "next": "Siguiente",
                 "previous": "Anterior"
             },
         }
     });

})
