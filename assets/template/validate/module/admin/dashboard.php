
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Dashboard del Usuario
                <small>ImporteApp</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
              <?php if(!empty($cajasactivas)):?>
                      <div class="row">
                          <?php
                              $estilocaja = "bg-green";
                              foreach($cajasactivas as $cajaactiva):?>
                                  <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box  <?php echo $estilocaja;?>">
                                      <div class="inner">
                                        <h3><?php echo $this->_formatnumero($cajaactiva->final);?></h3>
                                        <p><?php echo $cajaactiva->caja;?></p>
                                      </div>
                                      <div class="icon">
                                        <i class="ion ion-cash"></i>
                                      </div>
                                      <a href="<?php echo base_url()."cajas/aperturascajas/movimientos/".$cajaactiva->id."/".$cajaactiva->idcaja."";?>" class="small-box-footer">Ir a Movimientos <i class="fa fa-stack-overflow"></i></a>
                                    </div>
                                  </div>
                          <?php
                                      if($estilocaja=="bg-green"){ $estilocaja="bg-aqua"; }
                                      elseif($estilocaja=="bg-aqua"){ $estilocaja="bg-yellow"; }else{ $estilocaja="bg-green"; }
                             endforeach;?>
                      </div>
              <?php endif;?>
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
