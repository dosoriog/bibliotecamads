<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Usuarios_model extends CI_Model {
	public function getregistros($table,$fielorderby){
		$this->db->select("u.id,u.docepersona,u.usuagrupo,u.usuacambiarclave,u.usuanick,u.usuabodeent_df,u.usuabodesal_df,u.usuapuedecamb_bs,u.usuapuedecamb_be,u.usuaestado,u.tieneoperacion,u.borrado,p.docetipodocumento,p.docedocumento,CONCAT(p.doceprimerapellido,' ',p.docesegundoapellido,' ',p.doceprimernombre,' ',p.docesegundonombre) persona");
		$this->db->from($table." u");
		$this->db->join("rec_personas p","u.docepersona=p.id");
		$this->db->where("u.borrado","NO");
		$this->db->order_by("u.".$fielorderby,"ASC");
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getregistro($table,$id){
		$this->db->select("u.id,u.docepersona,u.usuagrupo,u.usuacambiarclave,u.usuanick,u.usuabodeent_df,u.usuabodesal_df,u.usuapuedecamb_bs,u.usuapuedecamb_be,u.usuaestado,u.tieneoperacion,u.borrado,p.docetipodocumento,p.docedocumento,CONCAT(p.doceprimerapellido,' ',p.docesegundoapellido,' ',p.doceprimernombre,' ',p.docesegundonombre) persona");
		$this->db->from($table." u");
		$this->db->join("rec_personas p","u.docepersona=p.id");
		$this->db->where("u.id",$id);
		$this->db->where("u.borrado","NO");
		$resultados = $this->db->get($table);
		return $resultados->row();
	}
	public function insert($table,$data){
		return $this->db->insert($table,$data);
	}
	public function update($table,$id,$data){
		$this->db->where("id",$id);
		return $this->db->update($table,$data);
	}
	public function updaterouserrol($table,$id,$data){
		$this->db->where("id",$id);
		return $this->db->update($table,$data);
	}
	public function updateroles($table,$id,$data){
		$this->db->where("rgrususuario",$id);
		return $this->db->update($table,$data);
	}
	public function getbodegas(){
		$this->db->select("id ccasevalue,bodenombre ccasenombre");
		$this->db->from("inv_bodegas");
		$this->db->where("borrado","NO");
		$this->db->where("bodeestado","ACTIVO");
		$this->db->order_by("bodenombre");
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getregistrouserrol($user,$rol){
		$this->db->from("seg_rgruposusuarios");
		$this->db->where("rgrususuario",$user);
		$this->db->where("rgrusgrupo",$rol);
		$resultados = $this->db->get();
		return $resultados->row();
	}
	public function getcajasactivas($usuario){
		$this->db->select("a.id,a.caapcaja idcaja,a.caja,a.caapinicio inicio,a.caapingresos ingresos,a.caapegresos egresos,a.caapfinal final");
		$this->db->from("vst_aperturas a");
		$this->db->where("a.caapusuario",$usuario);
		$this->db->where("a.borrado","NO");
		$this->db->where("a.caapestado","ACTIVO");
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getpersonas(){
		$this->db->select("id ccasevalue,CONCAT(doceprimerapellido,' ',docesegundoapellido,' ',doceprimernombre,' ',docesegundonombre) ccasenombre");
		$this->db->from("rec_personas");
		$this->db->where("borrado","NO");
	//	$this->db->where("docetipopersona","PERS");
		$this->db->where("doceusuario","SI");
		$this->db->where("doceestado","ACTIVO");
		$this->db->order_by("doceprimerapellido");
		$this->db->order_by("docesegundoapellido");
		$this->db->order_by("doceprimernombre");
		$this->db->order_by("docesegundonombre");
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getroles(){
		$this->db->select("id ccasevalue,grusnombre ccasenombre");
		$this->db->from("seg_gruposusuario");
		$this->db->where("borrado","NO");
		$this->db->where("grusestado","ACTIVO");
		$this->db->order_by("grusnombre","ASC");
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getrolesusuario($id){
		$this->db->select("rgrusgrupo field");
		$this->db->from("seg_rgruposusuarios");
		$this->db->where("borrado","NO");
		$this->db->where("rgrususuario",$id);
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getUsuarios(){
		$this->db->where("borrado","NO");
		$resultados = $this->db->get("seg_usuarios");
		return $resultados->result();
	}
	public function PRC_GetInicioSesion($usuario,$password){
        $query = $this->db->query("CALL GetInicioSesion('".$usuario."','".$password."')");
        if ($query->num_rows() > 0){
           $data = $query->row();    
           $query->free_result();
           $query->next_result();  
           return $data;
        }else{
           return false;
        }
    }
	public function login($usuario,$password)
	{
		$this->db->select("*");
		$this->db->from("TUsuarios u");

		$this->db->where("u.Activo","1");
		$this->db->where("u.Usuario",$usuario);
        $this->db->where("u.Clave",$password);
        $result = $this->db->get();

    if ($result->num_rows() > 0){
        return $result->row();
    }else{
        return false;
    }
	}
	public function getdmluser($usuario){
 	 $this->db->select("modulo,dml_select,dml_insert,dml_update,dml_delete,export");
	 $this->db->from("vst_userpermisos");
	 $this->db->where("usuanick",$usuario);
	 $this->db->where("(tipo_modulo<>'CAJ' AND tipo_modulo<>'CTA')");
 	 $resultados = $this->db->get();
 	 return $resultados->result();
  }
}
