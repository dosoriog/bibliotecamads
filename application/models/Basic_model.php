<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Basic_model extends CI_Model {

	public function getregistros($table,$select_val,$fielorderby,$orderby){
		$this->db->select($select_val);
		$this->db->order_by($fielorderby,$orderby);
		$resultados = $this->db->get($table);
		return $resultados->result();
	}
    
	public function getregistrosEjemplares($table,$select_val,$fielorderby,$orderby,$id){
		$this->db->select($select_val);
		$this->db->join("TEstados t","t.IDEstado=e.IDEstado");
		$this->db->where("e.IDMaterial",$id);
		$this->db->order_by($fielorderby,$orderby);
		$resultados = $this->db->get($table." e");
		return $resultados->result();
	}
	public function getregistro_countejemplar_prestamo($id){
		$this->db->select("COUNT(Codigo) num_prestar");
		$this->db->where("IDMaterial",$id);
		$this->db->where("Intercambio","NO");
		$resultados = $this->db->get("TEjemplares");
		return $resultados->row();
	}
	public function getregistro_countejemplar_intercambio($id){
		$this->db->select("COUNT(Codigo) num_intercambio");
		$this->db->where("IDMaterial",$id);
		$this->db->where("Intercambio","SI");
		$resultados = $this->db->get("TEjemplares");
		return $resultados->row();
	}
    public function getregistro($table,$fieldID,$id){
		$this->db->where($fieldID,$id);
		$resultados = $this->db->get($table);
		return $resultados->row();
	}
	public function getregistros_prestar($table,$select_val,$fielorderby,$orderby,$id){
		$this->db->select($select_val);
		$this->db->where("IDMaterial",$id);
		$this->db->where("IDEstado",4);
		$this->db->where("Intercambio","NO");
		$this->db->order_by($fielorderby,$orderby);
		$resultados = $this->db->get($table);
		return $resultados->result();
	}
    
	public function getregistros_devolver($id){
		$this->db->select("p.IdPrestamo AS ccasevalue, CONCAT(u.Nombre,' - ',e.Codigo) AS ccasenombre");
		$this->db->join("TEjemplares e","e.IDMaterial=m.IDMaterial");
		$this->db->join("TPrestamos p","p.Codigo=e.IDEjemplar");
		$this->db->join("TUsuarios u","u.IDUsuario=p.Idusuario");
		$this->db->where("m.IDMaterial",$id);
		$this->db->where("e.IDEstado",1002);
		$this->db->order_by("u.Nombre","ASC");
		$resultados = $this->db->get("TMateriales m");
		return $resultados->result();
	}
    public function getregistros_operacion(){
		$this->db->select("p.IdPrestamo,m.Titulo,e.Codigo,e.Intercambio,u.Nombre,p.Fechasolicitud,p.Estado,u.Idusuario,m.IDMaterial,p.Fechaprestamo,up.Nombre UPresta");
		$this->db->join("TPrestamos p","p.IDMaterial=m.IDMaterial");
		$this->db->join("TUsuarios u","u.IDUsuario=p.Idusuario");
		$this->db->join("TEjemplares e","e.IDEjemplar=p.Codigo","LEFT");
		$this->db->join("TUsuarios up","up.IDUsuario=p.Usuariopresta","LEFT");
		//$this->db->where("m.IDMaterial",$id);
		$this->db->where("p.Estado",4);
		$this->db->or_where("p.Estado",1003);
		$this->db->or_where("p.Estado",1002);
		$this->db->order_by("p.IdPrestamo","ASC");
		$resultados = $this->db->get("TMateriales m");
		return $resultados->result();
	}
	public function getregistros_busqueda($filtros){
		$this->db->select("*");
		//$this->db->where("*");
		$this->db->like('Titulo',$filtros);
		$this->db->order_by("Titulo","ASC");
		$resultados = $this->db->get("TMateriales");
		return $resultados->result();
	}
	
	public function getlistaselect($table,$value_ccasevalue,$value_ccasenombre,$fielorderby,$valida_estado,$value_estado){
			$this->db->select($value_ccasevalue." ccasevalue, ".$value_ccasenombre." ccasenombre");
			$this->db->where("borrado","NO");
			if($valida_estado=="SI"){
				$this->db->where($value_estado,"ACTIVO");
				}
			$this->db->order_by($fielorderby,"ASC");
			$resultados = $this->db->get($table);
			return $resultados->result();
	}
	public function insert($table,$data){
		return $this->db->insert($table,$data);
	}
	public function update($table,$fieldid,$id,$data){
		$this->db->where($fieldid,$id);
		return $this->db->update($table,$data);
	}
    public function delete($table,$fieldid,$id){
		$this->db->where($fieldid, $id);
        return $this->db->delete($table); 
	}
	
	
}
