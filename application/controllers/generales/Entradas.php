<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entradas extends CI_Controller {
   public function __construct(){
               parent::__construct();
               // INICIO se valida que la sesion esta activa actualemnte, en cas contrario se redirecciona al login
               
               // FIN se valida que la sesion esta activa actualemnte, en cas contrario se redirecciona al login
               $this->load->model("Basic_model");
               
   }

   // INICIO funcion index del controlador ----------------------------------------------------------------------------------
   public function index()
 	  {
        $data  = array(
          'coleccion' => $this->Basic_model->getregistros("TColecciones"," IDColeccion AS ccasevalue, Coleccion AS ccasenombre ","IDColeccion","ASC"),
          'autor' => $this->Basic_model->getregistros("TAutores"," IDAutor AS ccasevalue, Autor AS ccasenombre ","IDAutor","ASC"),
          'formato' => $this->Basic_model->getregistros("TFormatos"," IDFormato AS ccasevalue, Formato AS ccasenombre ","IDFormato","ASC"),
          'serie' => $this->Basic_model->getregistros("TSeries"," IDSerie AS ccasevalue, Serie AS ccasenombre ","IDSerie","ASC"),
          'ejemplares' => NULL,
          'usuario' =>  NULL,
          'ejemplar_prestar' => NULL,
          'ejemplar_devolver' => NULL,
          'registro_dat' => NULL,
          'accion' => "insert"
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('modulos/entradas/forma',$data);
        $this->load->view('layouts/footer');

  	}
        
// FIN funcion index del controlador ----------------------------------------------------------------------------------


// INICIO funcion view del controlador -------------------------------------------------------------------------------
    public function view($id){
      $this->_asignar_datacontrolle($this->config->item('nom_controller'),'view',$this->config->item('nom_controller').'view');
      $data  = array(
  			'registro' => $this->Basic_model->getregistro($this->config->item('table_controller'),$id),
  		);
  		$this->load->view($this->config->item('url_controller_view')."view",$data);
  	}
// FIN funcion view del controlador ----------------------------------------------------------------------------------


// INICIO funcion viewelimina del controlador -------------------------------------------------------------------------------
    public function viewelimina($id){
      $this->_asignar_datacontrolle($this->config->item('nom_controller'),'view',$this->config->item('nom_controller').'viewelimina');
      $data  = array(
  			'registro' => $this->Basic_model->getregistro($this->config->item('table_controller'),$id),
  		);
  		$this->load->view($this->config->item('url_controller_view')."viewelimina",$data);
  	}
// FIN funcion viewelimina del controlador ----------------------------------------------------------------------------------


// INICIO funcion add del controlador -------------------------------------------------------------------------------
    public function exec_busqueda(){
        $textbusqueda = $this->input->post("textbusqueda");
        $data  = array(
          'textbusqueda' => $textbusqueda,
          'registro_dat' => $this->Basic_model->getregistros_busqueda($textbusqueda),
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('modulos/busqueda/buscar',$data);
        $this->load->view('layouts/footer');
    }

    public function busqueda(){
          $data  = array(
            'textbusqueda' => NULL,
            'registro_dat' => NULL,
          );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('modulos/busqueda/buscar',$data);
        $this->load->view('layouts/footer');
  	}
// FIN funcion add del controlador ----------------------------------------------------------------------------------

public function buscar(){
       $id = $this->input->post("idbuscar");
       //$result = $this->Basic_model->getregistro("TMateriales","IDMaterial",$id);

       $this->db->where("IDMaterial",$id);
       $resultados = $this->db->get("TMateriales");
       //return $resultados->row();

       if($resultados->num_rows() > 0){
            $this->edit($id);
       }else{
            $this->session->set_flashdata("error","ID no encontrado.");
            $this->index();
       }
}


public function indexuser()
        {
       $data  = array(
         'registro_dat' => NULL,
         'accion' => "insertuser"
       );
       $this->load->view('layouts/header');
       $this->load->view('layouts/aside');
       $this->load->view('modulos/usuarios/forma',$data);
       $this->load->view('layouts/footer');

       }   
public function buscaruser(){
      $id = $this->input->post("idbuscar");
      //$result = $this->Basic_model->getregistro("TMateriales","IDMaterial",$id);

      $this->db->where("IDUsuario",$id);
      $resultados = $this->db->get("TUsuarios");
      //return $resultados->row();

      if($resultados->num_rows() > 0){
           $this->edituser($id);
      }else{
           $this->session->set_flashdata("error","ID no encontrado.");
           $this->indexuser();
      }
}

public function edituser($id){
      $data  = array(
          'registro_dat' => $this->Basic_model->getregistro("TUsuarios","IDUsuario",$id),
          'accion' => "updateuser"
      );

      $this->load->view('layouts/header');
      $this->load->view('layouts/aside');
      $this->load->view('modulos/usuarios/forma',$data);
      $this->load->view('layouts/footer');
    }

    public function insertuser(){
      $nombre = strtoupper($this->input->post("nombre"));
      $documento = $this->input->post("documento");
      $tipo = $this->input->post("tipo");
      $usuario = $this->input->post("usuario");
      $clave = $this->input->post("clave");
      $estado = $this->input->post("estado");


      //$usureg = $this->session->userdata("usuario");

      $data  = array(
            'Nombre' => $nombre,
            'Documento' => $documento,
            'IdTipoUsuario' => $tipo,
            'Usuario' => $usuario,
            'Clave' => $clave,
            'Activo' => $estado,
      );

      if ($this->Basic_model->insert("TUsuarios",$data)) {
                $id = $this->db->insert_id();
                $this->session->set_flashdata("result","Operacion Exitosa");
                $this->edituser($id);
        }
      else{
                $this->session->set_flashdata("error",$this->db->error());
                $this->indexuser();
        }
}
// FIN funcion insert del controlador ----------------------------------------------------------------------------------


// INICIO funcion update del controlador -------------------------------------------------------------------------------
public function updateuser(){
      $id = $this->input->post("id");
      $nombre = strtoupper($this->input->post("nombre"));
      $documento = $this->input->post("documento");
      $tipo = $this->input->post("tipo");
      $usuario = $this->input->post("usuario");
      $clave = $this->input->post("clave");
      $estado = $this->input->post("estado");


      //$usureg = $this->session->userdata("usuario");

      $data  = array(
            'Nombre' => $nombre,
            'Documento' => $documento,
            'IdTipoUsuario' => $tipo,
            'Usuario' => $usuario,
            'Clave' => $clave,
            'Activo' => $estado,
      );

        if ($this->Basic_model->update("TUsuarios","IDUsuario",$id,$data)) {
              $this->session->set_flashdata("result","Operacion Exitosa");
              $this->edituser($id);
        }
        else{
              $this->session->set_flashdata("error",$this->db->error());
              $this->indexuser();
        }
}



public function buscarm($id){
  //$id = $this->input->post("idbuscar");
  //$result = $this->Basic_model->getregistro("TMateriales","IDMaterial",$id);

  $this->db->where("IDMaterial",$id);
  $resultados = $this->db->get("TMateriales");
  //return $resultados->row();

  if($resultados->num_rows() > 0){
       $this->edit($id);
  }else{
       $this->session->set_flashdata("error","ID no encontrado.");
       $this->index();
  }
}

public function buscar_lista(){
  $id = $this->input->post("idlista");
  //$result = $this->Basic_model->getregistro("TMateriales","IDMaterial",$id);

  $this->db->where("IDMaterial",$id);
  $resultados = $this->db->get("TMateriales");
  //return $resultados->row();

  if($resultados->num_rows() > 0){
            $data  = array(
                  'coleccion' => $this->Basic_model->getregistros("TColecciones"," IDColeccion AS ccasevalue, Coleccion AS ccasenombre ","IDColeccion","ASC"),
                  'autor' => $this->Basic_model->getregistros("TAutores"," IDAutor AS ccasevalue, Autor AS ccasenombre ","IDAutor","ASC"),
                  'formato' => $this->Basic_model->getregistros("TFormatos"," IDFormato AS ccasevalue, Formato AS ccasenombre ","IDFormato","ASC"),
                  'serie' => $this->Basic_model->getregistros("TSeries"," IDSerie AS ccasevalue, Serie AS ccasenombre ","IDSerie","ASC"),
                  'ejemplares' => NULL,
                  'usuario' => $this->Basic_model->getregistros("TUsuarios"," IDUsuario AS ccasevalue, Nombre AS ccasenombre "," Nombre","ASC"),
                  'ejemplar_prestar' => $this->Basic_model->getregistros_prestar("TEjemplares"," IDEjemplar AS ccasevalue, Codigo AS ccasenombre "," Codigo","ASC",$id),
                  'ejemplar_devolver' => $this->Basic_model->getregistros_devolver($id),
                  'registro_dat' => $this->Basic_model->getregistro("TMateriales","IDMaterial",$id),
                  'accion' => "prestamo"
              );

              $this->load->view('layouts/header');
              $this->load->view('layouts/aside');
              $this->load->view('modulos/entradas/forma',$data);
              $this->load->view('layouts/footer');
  }else{
       $this->session->set_flashdata("error","ID no encontrado.");
       $this->index();
  }
}

public function buscar_ejemplares(){
      $id = $_REQUEST["idMaterial_busc"]; 
    
      $this->db->where("IDMaterial",$id);
      $this->db->where("IDEstado",1);
      $this->db->where("Intercambio","NO");
      $resultados = $this->db->get("TEjemplares");
      $registros = $resultados->result();
      
      $cadena = "<option value='' selected>Seleccionar</option>";
      foreach($registros as $registro){
            $cadena .= "<option value='".$registro->IDEjemplar."'>".$registro->Codigo."</option>";    
      }
      echo $cadena;
    }
    
public function buscar_ejemplares_canje(){
      $id = $_REQUEST["idMaterial_busc_canje"]; 
      $this->db->where("IDMaterial",$id);
      $this->db->where("IDEstado",1);
      $this->db->where("Intercambio","SI");
      $resultados = $this->db->get("TEjemplares");
      $registros = $resultados->result();
      
      $cadena = "<option value='' selected>Seleccionar</option>";
      foreach($registros as $registro){
            $cadena .= "<option value='".$registro->IDEjemplar."'>".$registro->Codigo."</option>";    
      }
      echo $cadena;
    }

public function buscar_lista_aux($id){
  //$id = $this->input->post("idlista");
  //$result = $this->Basic_model->getregistro("TMateriales","IDMaterial",$id);

  $this->db->where("IDMaterial",$id);
  $resultados = $this->db->get("TMateriales");
  //return $resultados->row();

  if($resultados->num_rows() > 0){
            $data  = array(
                  'coleccion' => $this->Basic_model->getregistros("TColecciones"," IDColeccion AS ccasevalue, Coleccion AS ccasenombre ","IDColeccion","ASC"),
                  'autor' => $this->Basic_model->getregistros("TAutores"," IDAutor AS ccasevalue, Autor AS ccasenombre ","IDAutor","ASC"),
                  'formato' => $this->Basic_model->getregistros("TFormatos"," IDFormato AS ccasevalue, Formato AS ccasenombre ","IDFormato","ASC"),
                  'serie' => $this->Basic_model->getregistros("TSeries"," IDSerie AS ccasevalue, Serie AS ccasenombre ","IDSerie","ASC"),
                  'ejemplares' => NULL,
                  'usuario' => $this->Basic_model->getregistros("TUsuarios"," IDUsuario AS ccasevalue, Nombre AS ccasenombre "," Nombre","ASC"),
                  'ejemplar_prestar' => $this->Basic_model->getregistros_prestar("TEjemplares"," IDEjemplar AS ccasevalue, Codigo AS ccasenombre "," Codigo","ASC",$id),
                  'ejemplar_devolver' => $this->Basic_model->getregistros_devolver($id),
                  'registro_dat' => $this->Basic_model->getregistro("TMateriales","IDMaterial",$id),
                  'accion' => "prestamo"
              );

              $this->load->view('layouts/header');
              $this->load->view('layouts/aside');
              $this->load->view('modulos/entradas/forma',$data);
              $this->load->view('layouts/footer');
  }else{
       $this->session->set_flashdata("error","ID no encontrado.");
       $this->index();
  }
}

// INICIO funcion edit del controlador -------------------------------------------------------------------------------
    public function edit($id){
          $data  = array(
              'coleccion' => $this->Basic_model->getregistros("TColecciones"," IDColeccion AS ccasevalue, Coleccion AS ccasenombre ","IDColeccion","ASC"),
              'autor' => $this->Basic_model->getregistros("TAutores"," IDAutor AS ccasevalue, Autor AS ccasenombre ","IDAutor","ASC"),
              'formato' => $this->Basic_model->getregistros("TFormatos"," IDFormato AS ccasevalue, Formato AS ccasenombre ","IDFormato","ASC"),
              'serie' => $this->Basic_model->getregistros("TSeries"," IDSerie AS ccasevalue, Serie AS ccasenombre ","IDSerie","ASC"),
              'ejemplares' => $this->Basic_model->getregistrosEjemplares("TEjemplares"," e.IDEjemplar, e.Codigo, e.Intercambio, e.IDEstado, t.Estado","Codigo","ASC",$id),
              'usuario' =>  NULL,
              'ejemplar_prestar' => NULL,
              'ejemplar_devolver' =>  NULL,
              'registro_dat' => $this->Basic_model->getregistro("TMateriales","IDMaterial",$id),
              'accion' => "update"
          );

          $this->load->view('layouts/header');
          $this->load->view('layouts/aside');
          $this->load->view('modulos/entradas/forma',$data);
          $this->load->view('layouts/footer');
  	}
// FIN funcion edit del controlador ----------------------------------------------------------------------------------


// INICIO funcion delete del controlador -------------------------------------------------------------------------------
    public function delete($id){
  		$data  = array(
        'swedit' => NULL,
  			'registro' => $this->Basic_model->getregistro($this->config->item('table_controller'),$id),
  		);
      $this->_asignar_datacontrolle($this->config->item('nom_controller'),'delete',$this->config->item('nom_controller').'delete');
      $this->_cargar_vistacontrolle($this->config->item('url_controller_view')."view_reg",$data,"1");
  	}
// FIN funcion delete del controlador ----------------------------------------------------------------------------------


public function solicitar_prestamo(){
      $id = $this->input->post("idlista");
      $material = $this->input->post("idmaterial");
      $usuario = $this->session->userdata("codigo");
      $data  = array(
            'Fechasolicitud' => date("y/m/d"),
            'Idusuario' => $usuario,
            'IDMaterial' => $id,
            'Estado' => 4,
      );
    
      if ($this->Basic_model->insert("TPrestamos",$data)) {
            $this->session->set_flashdata("result","Operacion Exitosa, Solicitud de  prestamo registrada: ".$material);
            header('Location: '.base_url().'index.php/generales/entradas/busqueda/');
      }
      else{
              $this->session->set_flashdata("error",$this->db->error());
              header('Location: '.base_url().'index.php/generales/entradas/busqueda/');
      }
    
    
    }
    
    public function operaciones(){
                  $data  = array(
                        'registro_dat' => $this->Basic_model->getregistros_operacion(),
                        'usuario' => $this->Basic_model->getregistros("TUsuarios"," IDUsuario AS ccasevalue, Nombre AS ccasenombre "," Nombre","ASC"),
                        'accion' => "operacion"
                  );
                  $this->load->view('layouts/header');
                  $this->load->view('layouts/aside');
                  $this->load->view('modulos/operaciones',$data);
                  $this->load->view('layouts/footer');
    }

    public function solicitar_canje(){
      $id = $this->input->post("idlista_canje");
      $material = $this->input->post("idmaterial_canje");
      $usuario = $this->session->userdata("codigo");
      $data  = array(
            'Fechasolicitud' => date("y/m/d"),
            'Idusuario' => $usuario,
            'IDMaterial' => $id,
            'Estado' => 1003,
      );
    
      if ($this->Basic_model->insert("TPrestamos",$data)) {
            $this->session->set_flashdata("result","Operacion Exitosa, Solicitud de canje registrada: ".$material);
            header('Location: '.base_url().'index.php/generales/entradas/busqueda/');
      }
      else{
              $this->session->set_flashdata("error",$this->db->error());
              header('Location: '.base_url().'index.php/generales/entradas/busqueda/');
      }
    
    
    }


    public function procesar_canje(){
      $id = $this->input->post("id_ope_cj");
      $ejemplar = $this->input->post("ejemplar_ope_cj");
    
      $data  = array(
            //'Fechasolicitud' => $fecha,
           // 'Idusuario' => $usuario,
            'Codigo' => $ejemplar,
            'Fechaprestamo' => date("y/m/d"),
            'Usuariopresta' => $this->session->userdata("codigo"),
            'Estado' => 1004,
      );
    
      if ($this->Basic_model->update("TPrestamos","IdPrestamo",$id,$data)) {
            $data  = array(
                  'IDEstado' => 1004,
            );
            $this->Basic_model->update("TEjemplares","IDEjemplar",$ejemplar,$data);
            $this->session->set_flashdata("result","Operacion Exitosa, Canje registrado");
            //$this->buscar_lista_aux($id);
            header('Location: '.base_url().'index.php/generales/entradas/operaciones/');
      }
      else{
              //$this->session->set_flashdata("error","Fecha: ".$fecha." - Usuario: ".$usuario." - Ejemplar: ".$ejemplar.",".$this->db->error());
              $this->session->set_flashdata("error",$this->db->error());
              //$this->index();
              //$this->buscar_lista_aux($id);
              header('Location: '.base_url().'index.php/generales/entradas/operaciones/');
      }
    
    
    }
    

public function procesar_prestamo(){
  $id = $this->input->post("id_ope_pr");
  //$fecha = $this->input->post("datepicker");
  //$usuario = $this->input->post("usuario_ope_pr");
  $ejemplar = $this->input->post("ejemplar_ope_pr");

  $data  = array(
        //'Fechasolicitud' => $fecha,
       // 'Idusuario' => $usuario,
        'Codigo' => $ejemplar,
        'Fechaprestamo' => date("y/m/d"),
        'Usuariopresta' => $this->session->userdata("codigo"),
        'Estado' => 1002,
  );

  if ($this->Basic_model->update("TPrestamos","IdPrestamo",$id,$data)) {
        $data  = array(
              'IDEstado' => 1002,
        );
        $this->Basic_model->update("TEjemplares","IDEjemplar",$ejemplar,$data);
        $this->session->set_flashdata("result","Operacion Exitosa, Prestamo registrado");
        //$this->buscar_lista_aux($id);
        header('Location: '.base_url().'index.php/generales/entradas/operaciones/');
  }
  else{
          //$this->session->set_flashdata("error","Fecha: ".$fecha." - Usuario: ".$usuario." - Ejemplar: ".$ejemplar.",".$this->db->error());
          $this->session->set_flashdata("error",$this->db->error());
          //$this->index();
          //$this->buscar_lista_aux($id);
          header('Location: '.base_url().'index.php/generales/entradas/operaciones/');
  }


}

public function procesar_ejemplar(){
      $id = $this->input->post("id_agre_ejemplar");
      $codigo = $this->input->post("codigo_ejemplar");
      $intercambio = $this->input->post("intercambio");


       $this->db->where("IDMaterial",$id);
       $this->db->where("Codigo",$codigo);
       $resultados = $this->db->get("TEjemplares");

       if($resultados->num_rows() <= 0){
                  $data  = array(
                        'Fecha' => date("y/m/d"),
                        'Ejemplar' => $id,
                        'Codigo' => $codigo,
                        'IDEstado' => 1,
                        'IDMaterial' => $id,
                        "Intercambio" => $intercambio,
                  );
            
                  if ($this->Basic_model->insert("TEjemplares",$data)) {

                        $result = $this->Basic_model->getregistro_countejemplar_prestamo($id);
                        $n_prestar= $result->num_prestar;

                        $result = $this->Basic_model->getregistro_countejemplar_intercambio($id);
                        $n_interc= $result->num_intercambio;

                        $data  = array(
                              'Ejemplares' => $n_prestar,
                              'Intercambio' => $n_interc,
                        );
                        $this->Basic_model->update("TMateriales","IDMaterial",$id,$data);
                  
                        $this->session->set_flashdata("result","Operacion Exitosa, Ejemplar registrado");
                        header('Location: '.base_url().'index.php/generales/entradas/buscarm/'.$id);
                  }
                  else{
                        $this->session->set_flashdata("error",$this->db->error());
                        header('Location: '.base_url().'index.php/generales/entradas/buscarm/'.$id);
                  }
      }else{
            $this->session->set_flashdata("error","Operacion No Exitosa, El codigo (".$codigo.") ya existe. Validar");
            header('Location: '.base_url().'index.php/generales/entradas/buscarm/'.$id);
      }

}

        public function procesar_devuelve(){
              $id = $this->input->post("id_ope_dv");
              //$fecha = $this->input->post("datepicker1");
              //$IdPrestamo = $this->input->post("usuario_ope_dv");

              $data  = array(
                    'Fechadevolucion' => date("y/m/d"),
                    'Usuariodevolucion' => $this->session->userdata("codigo"),
                    'Estado' => 1,
              );

              if ($this->Basic_model->update("TPrestamos","IdPrestamo",$id,$data)) {
                   
                    $result = $this->Basic_model->getregistro("TPrestamos","IdPrestamo",$IdPrestamo);
                    $idejemplar = $result->Codigo;
                    $data  = array(
                          'IDEstado' => 1,
                    );
                    $this->Basic_model->update("TEjemplares","IDEjemplar",$idejemplar,$data);
                    $this->session->set_flashdata("result","Operacion Exitosa, Devolucion registrada");
                    //$this->buscar_lista_aux($id);
                    header('Location: '.base_url().'index.php/generales/entradas/operaciones/');
              }
              else{
                      //$this->session->set_flashdata("error","Fecha: ".$fecha." - Usuario: ".$usuario." - Ejemplar: ".$ejemplar.",".$this->db->error());
                      $this->session->set_flashdata("error",$this->db->error());
                      //$this->index();
                      //$this->buscar_lista_aux($id);
                      header('Location: '.base_url().'index.php/generales/entradas/operaciones/');
              }


        }


        public function procesar_anular(){
            $id = $this->input->post("id_ope_an");
            //$fecha = $this->input->post("datepicker1");
            //$IdPrestamo = $this->input->post("usuario_ope_dv");

            $data  = array(
                  'Fechaanulacion' => date("y/m/d"),
                  'Usuarioanula' => $this->session->userdata("codigo"),
                  'Estado' => 1005,
            );

            if ($this->Basic_model->update("TPrestamos","IdPrestamo",$id,$data)) {
                 
                  $result = $this->Basic_model->getregistro("TPrestamos","IdPrestamo",$IdPrestamo);
                  $idejemplar = $result->Codigo;
                  $data  = array(
                        'IDEstado' => 1,
                  );
                  $this->Basic_model->update("TEjemplares","IDEjemplar",$idejemplar,$data);
                  $this->session->set_flashdata("result","Operacion Exitosa, Anulacion registrada");
                  //$this->buscar_lista_aux($id);
                  header('Location: '.base_url().'index.php/generales/entradas/operaciones/');
            }
            else{
                    //$this->session->set_flashdata("error","Fecha: ".$fecha." - Usuario: ".$usuario." - Ejemplar: ".$ejemplar.",".$this->db->error());
                    $this->session->set_flashdata("error",$this->db->error());
                    //$this->index();
                    //$this->buscar_lista_aux($id);
                    header('Location: '.base_url().'index.php/generales/entradas/operaciones/');
            }


      }

// INICIO funcion insert del controlador -------------------------------------------------------------------------------
    public function insert(){
                $titulo = strtoupper($this->input->post("titulo"));
                $autor = $this->input->post("autor");
                $coleccion = $this->input->post("coleccion");
                $ayyo = $this->input->post("ayyo");
                $isbn = strtoupper($this->input->post("isbn"));
                $issn = strtoupper($this->input->post("issn"));
                $institucion = strtoupper($this->input->post("institucion"));
                $formato = $this->input->post("formato");
                $ubicacion = strtoupper($this->input->post("ubicacion"));
                $serie = $this->input->post("serie");
                $imprenta = strtoupper($this->input->post("imprenta"));
                $audiencia = strtoupper($this->input->post("audiencia"));
                $resumen = $this->input->post("resumen");
                $hipervinculo = $this->input->post("hipervinculo");
                $cantidad_ing = $this->input->post("cantidad_ing");
                $cantidad_cang = $this->input->post("cantidad_cang");

                //$usureg = $this->session->userdata("usuario");

                $data  = array(
                      'Titulo' => $titulo,
                      'IDColeccion' => $coleccion,
                      'Anyo' => $ayyo,
                      'ISBN' => $isbn,
                      'ISSN' => $issn,
                      'Autor' => $autor,
                      'Institucion' => $institucion,
                      'IDFormato' => $formato,
                      'Imprenta' => $imprenta,
                      'IDSerie' => $serie,
                      'Resumen' => $resumen,
                      'Audiencia' => $audiencia,
                      'Hipervinvulo' => $hipervinculo,
                      'Ejemplares' => $cantidad_ing,
                      'Intercambio' => $cantidad_cang,
                      'IDPais' => 'COL',
                      'IDIdioma' => 1,
                );
      
                if ($this->Basic_model->insert("TMateriales",$data)) {
                          $id = $this->db->insert_id();
                          $this->session->set_flashdata("result","Operacion Exitosa");
                          $this->edit($id);
                  }
                else{
                          $this->session->set_flashdata("error",$this->db->error());
                          $this->index();
                  }
  	}
// FIN funcion insert del controlador ----------------------------------------------------------------------------------


// INICIO funcion update del controlador -------------------------------------------------------------------------------
    public function update(){
  		$id = $this->input->post("id");
            $titulo = strtoupper($this->input->post("titulo"));
            $autor = $this->input->post("autor");
            $coleccion = $this->input->post("coleccion");
            $ayyo = $this->input->post("ayyo");
            $isbn = strtoupper($this->input->post("isbn"));
            $issn = strtoupper($this->input->post("issn"));
            $institucion = strtoupper($this->input->post("institucion"));
            $formato = $this->input->post("formato");
            $ubicacion = strtoupper($this->input->post("ubicacion"));
            $serie = $this->input->post("serie");
            $imprenta = strtoupper($this->input->post("imprenta"));
            $audiencia = strtoupper($this->input->post("audiencia"));
            $resumen = $this->input->post("resumen");
            $hipervinculo = $this->input->post("hipervinculo");
            $cantidad_ing = $this->input->post("cantidad_ing");
            $cantidad_cang = $this->input->post("cantidad_cang");

                //$usureg = $this->session->userdata("usuario");

                  $data  = array(
                        'Titulo' => $titulo,
                        'IDColeccion' => $coleccion,
                        'Anyo' => $ayyo,
                        'ISBN' => $isbn,
                        'ISSN' => $issn,
                        'Autor' => $autor,
                        'Institucion' => $institucion,
                        'IDFormato' => $formato,
                        'Imprenta' => $imprenta,
                        'IDSerie' => $serie,
                        'Resumen' => $resumen,
                        'Audiencia' => $audiencia,
                        'Hipervinvulo' => $hipervinculo,
                        'Ejemplares' => $cantidad_ing,
                        'Intercambio' => $cantidad_cang,
                        'IDPais' => 'COL',
                        'IDIdioma' => 1,
                  );

                  if ($this->Basic_model->update("TMateriales","IDMaterial",$id,$data)) {
                        $this->session->set_flashdata("result","Operacion Exitosa");
                        $this->edit($id);
                  }
                  else{
                        $this->session->set_flashdata("error",$this->db->error());
                        $this->index();
                  }
}
// FIN funcion update del controlador ----------------------------------------------------------------------------------


// INICIO funcion deletef del controlador -------------------------------------------------------------------------------
    public function deletef(){
      $id = $this->input->post("id");
      //$usureg = $this->session->userdata("usuario");
  		$data  = array(
        'borrado' => 'SI',
        'usuarioeli' => $usureg,
        'fechaeli' => date('Y-m-d'),
        'horaeli' => date('Y-m-d H:i:s')
  		);
  		if ($this->Basic_model->update($this->config->item('table_controller'),$id,$data)) {
        $this->_asignar_datacontrolle($this->config->item('nom_controller'),'delete','conceptosindex');
        redirect(base_url().$this->config->item('url_controller'));
  		}
  		else{
        $this->_asignar_datacontrolle($this->config->item('nom_controller'),'delete','delete');
        $this->session->set_flashdata("error","No se pudo guardar la informacion");
  			redirect(base_url().$this->config->item('url_controller')."delete/".$id);
  		}
  	}
// FIN funcion deletef del controlador ----------------------------------------------------------------------------------


// INICIO funcion excel del controlador -------------------------------------------------------------------------------
public function excel() {
    $value_date = date('Y-m-d H:i:s');
    $this->load->helper('exportexcel');
    $namaFile = $this->config->item('title_index')." - ".$value_date.".xls";
    $judul = $this->config->item('title_index');
    $tablehead = 0;
    $tablebody = 1;
    $nourut = 1;
    //penulisan header
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header("Content-Disposition: attachment;filename=" . $namaFile . "");
    header("Content-Transfer-Encoding: binary ");

    xlsBOF();

    $kolomhead = 0;
    xlsWriteLabel($tablehead, $kolomhead++, "#");
    xlsWriteLabel($tablehead, $kolomhead++, "Nombre");
    xlsWriteLabel($tablehead, $kolomhead++, "Permite $ Ing");
    xlsWriteLabel($tablehead, $kolomhead++, "Permite $ Egr");
    xlsWriteLabel($tablehead, $kolomhead++, "Apertura Aut.");
    xlsWriteLabel($tablehead, $kolomhead++, "Estado");

    foreach ($this->Basic_model->getregistros($this->config->item('table_controller'),$this->config->item('field_orderby')) as $data) {
        $kolombody = 0;

        //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
        xlsWriteNumber($tablebody, $kolombody++, $data->id);
        xlsWriteLabel($tablebody, $kolombody++, $data->cajanombre);
        xlsWriteLabel($tablebody, $kolombody++, $data->cajamanejaingresos);
        xlsWriteLabel($tablebody, $kolombody++, $data->cajamenejaegresos);
        xlsWriteLabel($tablebody, $kolombody++, $data->cajaabrirsigapertura_aut);
        xlsWriteLabel($tablebody, $kolombody++, $data->cajaestado);

        $tablebody++;
        $nourut++;
    }

    xlsEOF();
    exit();
}
// FIN funcion excel del controlador -------------------------------------------------------------------------------

}
