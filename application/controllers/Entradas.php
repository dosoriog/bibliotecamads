<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entradas extends CI_Controller {
   public function __construct(){
               parent::__construct();
               // INICIO se valida que la sesion esta activa actualemnte, en cas contrario se redirecciona al login
               
               // FIN se valida que la sesion esta activa actualemnte, en cas contrario se redirecciona al login

               
   }

   // INICIO funcion index del controlador ----------------------------------------------------------------------------------
   public function index()
 	  {
       /*$this->_asignar_datacontrolle($this->config->item('nom_controller'),'index',$this->config->item('nom_controller').'index');
       $data  = array(
 		   	           'registros' => $this->Basic_model->getregistros($this->config->item('table_controller'),$this->config->item('field_orderby')),
 	           	);
       $this->_cargar_vistacontrolle($this->config->item('url_controller_view')."index",$data,"1");*/
       $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('welcome_message');
        $this->load->view('layouts/footer');

  	}
// FIN funcion index del controlador ----------------------------------------------------------------------------------


// INICIO funcion view del controlador -------------------------------------------------------------------------------
    public function view($id){
      $this->_asignar_datacontrolle($this->config->item('nom_controller'),'view',$this->config->item('nom_controller').'view');
      $data  = array(
  			'registro' => $this->Basic_model->getregistro($this->config->item('table_controller'),$id),
  		);
  		$this->load->view($this->config->item('url_controller_view')."view",$data);
  	}
// FIN funcion view del controlador ----------------------------------------------------------------------------------


// INICIO funcion viewelimina del controlador -------------------------------------------------------------------------------
    public function viewelimina($id){
      $this->_asignar_datacontrolle($this->config->item('nom_controller'),'view',$this->config->item('nom_controller').'viewelimina');
      $data  = array(
  			'registro' => $this->Basic_model->getregistro($this->config->item('table_controller'),$id),
  		);
  		$this->load->view($this->config->item('url_controller_view')."viewelimina",$data);
  	}
// FIN funcion viewelimina del controlador ----------------------------------------------------------------------------------


// INICIO funcion add del controlador -------------------------------------------------------------------------------
    public function add(){
      $data  = array(
        'swedit' => NULL,
        'registro' => NULL,
  		);
      $this->_asignar_datacontrolle($this->config->item('nom_controller'),'add',$this->config->item('nom_controller').'add');
      $this->_cargar_vistacontrolle($this->config->item('url_controller_view')."view_reg",$data,"1");
  	}
// FIN funcion add del controlador ----------------------------------------------------------------------------------


// INICIO funcion edit del controlador -------------------------------------------------------------------------------
    public function edit($id,$sw){
  		$data  = array(
        'swedit' => $sw,
        'registro' => $this->Basic_model->getregistro($this->config->item('table_controller'),$id),
  		);
      $this->_asignar_datacontrolle($this->config->item('nom_controller'),'edit',$this->config->item('nom_controller').'edit');
      $this->_cargar_vistacontrolle($this->config->item('url_controller_view')."view_reg",$data,"1");
  	}
// FIN funcion edit del controlador ----------------------------------------------------------------------------------


// INICIO funcion delete del controlador -------------------------------------------------------------------------------
    public function delete($id){
  		$data  = array(
        'swedit' => NULL,
  			'registro' => $this->Basic_model->getregistro($this->config->item('table_controller'),$id),
  		);
      $this->_asignar_datacontrolle($this->config->item('nom_controller'),'delete',$this->config->item('nom_controller').'delete');
      $this->_cargar_vistacontrolle($this->config->item('url_controller_view')."view_reg",$data,"1");
  	}
// FIN funcion delete del controlador ----------------------------------------------------------------------------------


// INICIO funcion insert del controlador -------------------------------------------------------------------------------
    public function insert(){
  		$nombre = strtoupper($this->input->post("nombre"));
  		$mingresos = $this->input->post("mingresos")==TRUE? 'SI':'NO';
      $megresos = $this->input->post("megresos")==TRUE? 'SI':'NO';
      $apertaut = $this->input->post("aperturaaut")==TRUE? 'SI':'NO';
      $estado = $this->input->post("estado")==TRUE? 'ACTIVO':'INACTIVO';
      $usureg = $this->session->userdata("usuario");

      $this->form_validation->set_rules("nombre","Nombre","required|min_length[5]|max_length[50]",
           array('required' => "Debe ingresar un nombre para la caja entre 5 y 50 caracteres",
                 'min_length' => "Debe ingresar un nombre para la caja entre 5 y 50 caracteres",
                 'max_length' => "Debe ingresar un nombre para la caja entre 5 y 50 caracteres")
       );
      if ($this->form_validation->run()) {
           $data  = array(
  		     	'cajanombre' => $nombre,
            'cajamanejaingresos' => $mingresos,
            'cajamenejaegresos' => $megresos,
            'cajaabrirsigapertura_aut' => $apertaut,
            'cajaestado' => $estado,
            'tieneoperacion' => "SI",
            'usuarioreg' => $usureg
  		    );
  		   if ($this->Basic_model->insert($this->config->item('table_controller'),$data)) {
               $id_caja = $this->db->insert_id();
               $data  = array(
                  'id' => "CAJ_".$id_caja,
                  'modunombre' => $nombre,
                  'modutipo' => "CAJ",
                  'moducodkey' => $id_caja,
                  'tieneoperacion' => "SI"
               );
              $this->Basic_model->insert("seg_modulos",$data);
              $this->_asignar_datacontrolle($this->config->item('nom_controller'),'insert','conceptosindex');
              redirect(base_url().$this->config->item('url_controller'));
  		     }
  		   else{
              $this->_asignar_datacontrolle($this->config->item('nom_controller'),'insert','add');
              $this->session->set_flashdata("error","No se pudo guardar la informacion");
  			      redirect(base_url().$this->config->item('url_controller')."add");
  		     }
         }
      else{
     			$this->add();
     		}
  	}
// FIN funcion insert del controlador ----------------------------------------------------------------------------------


// INICIO funcion update del controlador -------------------------------------------------------------------------------
    public function update(){
  		$id = strtoupper($this->input->post("id"));
      $nombre = strtoupper($this->input->post("nombre"));
  		$mingresos = $this->input->post("mingresos")==TRUE? 'SI':'NO';
      $megresos = $this->input->post("megresos")==TRUE? 'SI':'NO';
      $apertaut = $this->input->post("aperturaaut")==TRUE? 'SI':'NO';
      $estado = $this->input->post("estado")==TRUE? 'ACTIVO':'INACTIVO';

      $this->form_validation->set_rules("nombre","Nombre","required|min_length[5]|max_length[50]",
           array('required' => "Debe ingresar un nombre para la caja entre 5 y 50 caracteres",
                 'min_length' => "Debe ingresar un nombre para la caja entre 5 y 50 caracteres",
                 'max_length' => "Debe ingresar un nombre para la caja entre 5 y 50 caracteres")
       );
      if ($this->form_validation->run()) {
            $data  = array(
                   'cajanombre' => $nombre,
                   'cajamanejaingresos' => $mingresos,
                   'cajamenejaegresos' => $megresos,
                   'cajaabrirsigapertura_aut' => $apertaut,
                   'cajaestado' => $estado
             );
  		       if ($this->Basic_model->update($this->config->item('table_controller'),$id,$data)) {
                 $id_caja = $this->db->insert_id();
                 $data  = array(
                      'modunombre' => $nombre,
                   );
                 $this->Datos_model->update_name_modulo("CAJ",$id,$data);
                 $this->_asignar_datacontrolle($this->config->item('nom_controller'),'update','conceptosindex');
                 redirect(base_url().$this->config->item('url_controller'));
  		         }
  		       else{
                $this->_asignar_datacontrolle($this->config->item('nom_controller'),'update','edit');
                $this->session->set_flashdata("error","No se pudo guardar la informacion");
  			        redirect(base_url().$this->config->item('url_controller')."edit/".$id);
  		      }
          }
       else{
           $this->edit($id,2);
         }
  	}
// FIN funcion update del controlador ----------------------------------------------------------------------------------


// INICIO funcion deletef del controlador -------------------------------------------------------------------------------
    public function deletef(){
      $id = strtoupper($this->input->post("id"));
      $usureg = $this->session->userdata("usuario");
  		$data  = array(
        'borrado' => 'SI',
        'usuarioeli' => $usureg,
        'fechaeli' => date('Y-m-d'),
        'horaeli' => date('Y-m-d H:i:s')
  		);
  		if ($this->Basic_model->update($this->config->item('table_controller'),$id,$data)) {
        $this->_asignar_datacontrolle($this->config->item('nom_controller'),'delete','conceptosindex');
        redirect(base_url().$this->config->item('url_controller'));
  		}
  		else{
        $this->_asignar_datacontrolle($this->config->item('nom_controller'),'delete','delete');
        $this->session->set_flashdata("error","No se pudo guardar la informacion");
  			redirect(base_url().$this->config->item('url_controller')."delete/".$id);
  		}
  	}
// FIN funcion deletef del controlador ----------------------------------------------------------------------------------


// INICIO funcion excel del controlador -------------------------------------------------------------------------------
public function excel() {
    $value_date = date('Y-m-d H:i:s');
    $this->load->helper('exportexcel');
    $namaFile = $this->config->item('title_index')." - ".$value_date.".xls";
    $judul = $this->config->item('title_index');
    $tablehead = 0;
    $tablebody = 1;
    $nourut = 1;
    //penulisan header
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header("Content-Disposition: attachment;filename=" . $namaFile . "");
    header("Content-Transfer-Encoding: binary ");

    xlsBOF();

    $kolomhead = 0;
    xlsWriteLabel($tablehead, $kolomhead++, "#");
    xlsWriteLabel($tablehead, $kolomhead++, "Nombre");
    xlsWriteLabel($tablehead, $kolomhead++, "Permite $ Ing");
    xlsWriteLabel($tablehead, $kolomhead++, "Permite $ Egr");
    xlsWriteLabel($tablehead, $kolomhead++, "Apertura Aut.");
    xlsWriteLabel($tablehead, $kolomhead++, "Estado");

    foreach ($this->Basic_model->getregistros($this->config->item('table_controller'),$this->config->item('field_orderby')) as $data) {
        $kolombody = 0;

        //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
        xlsWriteNumber($tablebody, $kolombody++, $data->id);
        xlsWriteLabel($tablebody, $kolombody++, $data->cajanombre);
        xlsWriteLabel($tablebody, $kolombody++, $data->cajamanejaingresos);
        xlsWriteLabel($tablebody, $kolombody++, $data->cajamenejaegresos);
        xlsWriteLabel($tablebody, $kolombody++, $data->cajaabrirsigapertura_aut);
        xlsWriteLabel($tablebody, $kolombody++, $data->cajaestado);

        $tablebody++;
        $nourut++;
    }

    xlsEOF();
    exit();
}
// FIN funcion excel del controlador -------------------------------------------------------------------------------

}
