<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
   public function __construct(){
    parent::__construct();

    $this->load->model("Usuarios_model");

   }
   public function index(){
      if($this->session->userdata("login")){
              if($this->session->userdata("cambiarclave")=="SI"){
                        $this->session->sess_destroy();
                        redirect(base_url());
              }
              else{
                          $data  = array(
                              'cajasactivas' => NULL, //$this->Usuarios_model->getcajasactivas($this->session->userdata("usuario")),
                          );
                          $this->load->view('layouts/header');
                          $this->load->view('layouts/aside');
                          $this->load->view('modulos/busqueda/buscar');
                          $this->load->view('layouts/footer');
                  }
      }else{
          $this->load->view("modulos/login");
      }
   }
   public function login(){
      $usuario  = $this->input->post("usuario");
      $password = $this->input->post("password");
      //$result   = $this->Usuarios_model-login($usuario,sha1($password));
      $result   = $this->Usuarios_model->login($usuario,$password);
      if(!$result){
         $this->session->set_flashdata("error","Usuario y/o contraseña incorrectos.");
         redirect(base_url());
      }else{
        
        $data = array(
              'codigo' => $result->IDUsuario,
              'nombre' => $result->Nombre,
              'usuario' => $usuario,
              'cambiarclave' => "NO",
              'login' => true,
              'rol' => $result->IdTipoUsuario,
        );
        $this->session->set_userdata($data);

        if($result->usuacambiarclave=="SI"){
                    $data  = array(
                        'registro' => $result,
                        'swedit' => 1,
                        'swok' => 0,
                    );
                    $this->config->set_item('url_controller', 'seguridad/usuarios/');
                    $this->config->set_item('url_controller_view', 'admin/seguridad/usuarios/');
                    $this->config->set_item('nom_controller', 'usuarios');
                    $this->_asignar_datacontrolle($this->config->item('nom_controller'),'edit',$this->config->item('nom_controller').'edit');
                    $this->load->view('layouts/header');
                    $this->load->view('admin/cambiarclave',$data);
                    $this->load->view('layouts/footer');
        }
        else{
                    $data  = array(
                        'registro' => NULL,
                    );
                    $this->load->view('layouts/header');
                    $this->load->view('layouts/aside');
                    $this->load->view('modulos/busqueda/buscar');
                    $this->load->view('layouts/footer');
        }
      }
   }
  public function logout(){
      $this->session->sess_destroy();
      redirect(base_url());
   }
}
