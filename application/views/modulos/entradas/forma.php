
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php 
            $estadocontrol = "";
            if($accion=="prestamo"){
                $estadocontrol = "disabled";
            }
        
        ?>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box ">
            
            <div class="box-header   ">
                    <h3 class="box-title">Entradas</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                        </div>
            </div>

        <div class="box-body">
                        <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/<?php echo $accion;?>" method="POST">
                                <input  type="hidden" name="id" id="id" value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->IDMaterial); ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if($this->session->flashdata("error")):?>
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                                            </div>
                                        <?php endif;?>
                                        <?php if($this->session->flashdata("result")):?>
                                            <div class="alert alert-success alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("result"); ?></p>
                                            </div>
                                        <?php endif;?>
                                   <div class="row">
                                      <div class="col-md-8">      
                                                            <div class="row">
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                    <label for="nombre">ID:</label>
                                                                                    <input  maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->IDMaterial); ?>" type="text" class="form-control" disabled>
                                                                            </div>    
                                                                        </div>
                                                                        <div class="col-md-10">
                                                                            <div class="form-group">
                                                                                    <label for="nombre">Titulo:</label>
                                                                                    <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Titulo); ?>" type="text" class="form-control" id="titulo" name="titulo" <?php echo $estadocontrol;?>>
                                                                            </div>    
                                                                        </div>
                                                            </div>
                                                            <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                        <label>Autor:</label>
                                                                                        <select autocomplete="off" required class="form-control select2" style="width: 100%;" id="autor" name="autor" <?php echo $estadocontrol;?>>
                                                                                                <option value="" selected>Seleccionar</option>
                                                                                                <?php $valuetipo=(is_null($registro_dat)?NULL:$registro_dat->Autor);
                                                                                                foreach($autor as $registro):?>
                                                                                                    <?php if($registro->ccasevalue == $valuetipo):?>
                                                                                                    <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                                                <?php else:?>
                                                                                                    <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                                                    <?php endif;?>
                                                                                                <?php endforeach;?>
                                                                                        </select>
                                                                                    </div>   
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label>Coleccion:</label>
                                                                                        <select autocomplete="off" required class="form-control select2" style="width: 100%;" id="coleccion" name="coleccion" <?php echo $estadocontrol;?>>
                                                                                                <option value="" selected>Seleccionar</option>
                                                                                                <?php $valuetipo=(is_null($registro_dat)?NULL:$registro_dat->IDColeccion);
                                                                                                foreach($coleccion as $registro):?>
                                                                                                    <?php if($registro->ccasevalue == $valuetipo):?>
                                                                                                    <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                                                <?php else:?>
                                                                                                    <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                                                    <?php endif;?>
                                                                                                <?php endforeach;?>
                                                                                        </select>
                                                                                    </div> 
                                                                            </div>
                                                            </div>
                                                            <div class="row">
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                    <label for="nombre">Año:</label>
                                                                                    <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Anyo); ?>" type="text" class="form-control" id="ayyo" name="ayyo" <?php echo $estadocontrol;?>>
                                                                            </div>    
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                            <div class="form-group">
                                                                                    <label for="nombre">ISBN:</label>
                                                                                    <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->ISBN); ?>" type="text" class="form-control" id="isbn" name="isbn" <?php echo $estadocontrol;?>>
                                                                            </div>    
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                            <div class="form-group">
                                                                                    <label for="nombre">ISSN:</label>
                                                                                    <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->ISSN); ?>" type="text" class="form-control" id="issn" name="issn" <?php echo $estadocontrol;?>>
                                                                            </div>    
                                                                            </div>
                                                            </div>
                                                            <div class="row">
                                                                        <div class="col-md-8">
                                                                            <div class="form-group">
                                                                                    <label for="nombre">Institucion:</label>
                                                                                    <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Institucion); ?>" type="text" class="form-control" id="institucion" name="institucion" <?php echo $estadocontrol;?>>
                                                                            </div>    
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                            <label>Formato:</label>
                                                                                            <select autocomplete="off" required class="form-control select2" style="width: 100%;" id="formato" name="formato" <?php echo $estadocontrol;?>>
                                                                                                    <option value="" selected>Seleccionar</option>
                                                                                                    <?php $valuetipo=(is_null($registro_dat)?NULL:$registro_dat->IDFormato);
                                                                                                    foreach($formato as $registro):?>
                                                                                                        <?php if($registro->ccasevalue == $valuetipo):?>
                                                                                                        <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                                                    <?php else:?>
                                                                                                        <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                                                        <?php endif;?>
                                                                                                    <?php endforeach;?>
                                                                                            </select>  
                                                                                </div>  
                                                                            </div>
                                                            </div>
                                                            <div class="row">
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                    <label for="nombre">Ubicacion fisica:</label>
                                                                                    <input autocomplete="off" required maxlength="50"  value="" type="text" class="form-control" id="ubicacion" name="ubicacion" <?php echo $estadocontrol;?>>
                                                                            </div>    
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                            <label>Serie:</label>
                                                                                            <select autocomplete="off" required class="form-control select2" style="width: 100%;"  id="serie" name="serie" <?php echo $estadocontrol;?>>
                                                                                                    <option value="" selected>Seleccionar</option>
                                                                                                    <?php $valuetipo=(is_null($registro_dat)?NULL:$registro_dat->IDSerie);
                                                                                                    foreach($serie as $registro):?>
                                                                                                        <?php if($registro->ccasevalue == $valuetipo):?>
                                                                                                        <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                                                    <?php else:?>
                                                                                                        <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                                                        <?php endif;?>
                                                                                                    <?php endforeach;?>
                                                                                            </select>  
                                                                                </div>  
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                    <label for="nombre">Imprenta:</label>
                                                                                    <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Imprenta); ?>" type="text" class="form-control" id="imprenta" name="imprenta" <?php echo $estadocontrol;?>>
                                                                            </div>    
                                                                            </div>
                                                            </div>
                                      </div>
                                      <div class="col-md-4"> 
                                            <div class="row">
                                                    <div class="form-group col-md-4 col-12 text-center">
                                                        <input  type="hidden" name="foto_carnet" id="foto_carnet" value="">
                                                        <div class="upload mt-4 pr-md-4 rounded-circle" style="width:300px;height:300px;margin:auto;">
                                                        <input  type="file" onchange="uploadImage('#files');" id="files" class="dropify rounded-circle" data-default-file="" data-max-file-size="2M" />
                                                        <p class="mt-2"><img src="<?=base_url()?>assets/img/icono_atleta_mini.jpg"> Imagen portada</p>
                                                        </div>
                                                    </div>
                                            </div>         
                                      </div>    
                                    </div>

                                        <div class="row">
                                                       <div class="col-md-6">
                                                           <div class="form-group">
                                                                <label for="nombre">Resumen:</label>
                                                                <textarea class="form-control" rows="8"  id="resumen" name="resumen" <?php echo $estadocontrol;?>><?php echo (is_null($registro_dat)?NULL:$registro_dat->Resumen); ?></textarea>
                                                           </div>    
                                                        </div>
                                                        <div class="col-md-6">
                                                              <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                                <label for="nombre">Audiencia:</label>
                                                                                <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Audiencia); ?>" type="text" class="form-control" id="audiencia" name="audiencia" <?php echo $estadocontrol;?>>
                                                                        </div>    
                                                                     </div>
                                                              </div> 
                                                              <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                                <label for="nombre">Hipervinculo:</label>
                                                                                <input autocomplete="off" maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Hipervinvulo); ?>" type="text" class="form-control" id="hipervinculo" name="hipervinculo" <?php echo $estadocontrol;?>>
                                                                        </div>    
                                                                     </div>
                                                              </div> 
                                                              <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                                <label for="nombre">Asignado a prestamo:</label>
                                                                                <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Ejemplares); ?>" type="text" class="form-control" id="cantidad_ing" name="cantidad_ing" disabled>
                                                                        </div>    
                                                                     </div>
                                                                     <div class="col-md-6">
                                                                        <div class="form-group">
                                                                                <label for="nombre">Asignado a canje:</label>
                                                                                <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Intercambio); ?>" type="text" class="form-control" id="cantidad_cang" name="cantidad_cang" disabled>
                                                                        </div>    
                                                                     </div>
                                                              </div>         
                                                        </div>
                                                        
                                        </div>
                                        <?php if($accion=="prestamo"){ ?>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-operacion_p" ><span class="fa  fa-book"></span> Prestamos</button>
                                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-operacion_d" ><span class="fa  fa-book"></span> Devolucion</button>
                                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-operacion_i" ><span class="fa  fa-book"></span> Intercambio</button>
                                        <?php } else { ?>  
                                                    <a class="btn btn-primary" href="<?php echo base_url();?>" role="button"><span class="fa fa-search"></span> Buscar</a> 
                                                    <?php if(($this->session->userdata("rol")==4) || ($this->session->userdata("rol")==3)){ ?>
                                                            <button type="submit" class="btn btn-success"><span class="fa  fa-check"></span> Guardar</button>
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-ejemplares"><span class="fa fa-book"></span> Ejemplares</button>
                                                            <a class="btn btn-warning" href="<?php echo base_url();?>index.php/generales/entradas" role="button"><span class="fa fa-plus"></span> Nuevo</a>      
                                                    <?php }  ?>              
                                        <?php } ?>                
                                    </div>
                                </div>
                            
                        </form>
        </div>
						        
	</div>                
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Buscar...</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/buscar" method="POST">
            <div class="modal-body">
                <div class="col-md-12">      
                            <div class="row">
                                    
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                    <label for="nombre">ID:</label>
                                                    <input required maxlength="10"  type="text" class="form-control" id="idbuscar" name="idbuscar">
                                            </div>    
                                        </div>
                            </div>
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" ><span class="fa  fa-search"></span> Buscar</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-ejemplares">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ejemplares agregados</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/procesar_ejemplar" method="POST">
            <input  type="hidden" name="id_agre_ejemplar" id="id_agre_ejemplar" value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->IDMaterial); ?>">
            <div class="modal-body">
                <div class="col-md-12">      
                            <div class="row">
                                            <div class="col-md-3">
                                                    <div class="form-group">
                                                            <label for="nombre">Codigo:</label>
                                                            <input autocomplete="off" required maxlength="20"  value="" type="text" class="form-control" id="codigo_ejemplar" name="codigo_ejemplar">
                                                    </div>    
                                            </div> 
                            
                                            <div class="col-md-4">
                                                        <div class="form-group">
                                                                    <label>Tipo:</label>
                                                                    <select autocomplete="off" required class="form-control select2" style="width: 100%;"  id="intercambio" name="intercambio" >
                                                                           <option value="NO" selected>Prestamo</option>
                                                                           <option value="SI">Canje</option>
                                                                    </select>  
                                                        </div>  
                                                                               
                                                    
                                            </div>              
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                        <table id="listregistros" class="table table-striped table-condensed table-bordered">
                                            <thead>
                                                <tr>
                                                    <!-- Inicio Agregar solo las columnas que se quieran mostrar -->
                                                    <th>ID</th>
                                                    <th>Codigo</th>
                                                    <th>Tipo</th>
                                                    <th>Estado</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(!empty($ejemplares)):?>
                                                    <?php foreach($ejemplares as $registro):?>
                                                        <tr> 
                                                            <td><?php echo $registro->IDEjemplar;?></td>
                                                            <td><?php echo $registro->Codigo;?></td>
                                                            <td><?php if($registro->Intercambio=="NO"){ echo "Prestamo"; } else { echo "Canje"; }?></td>
                                                            <td><?php echo $registro->Estado;?></td>
                                                        </tr>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </tbody>
                                        </table>         
                                    </div>       
                                </div>    
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-success" ><span class="fa  fa-check"></span> Agregar</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-operacion_p">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realizar operacion prestamos</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/procesar_prestamo" method="POST">
            <input  type="hidden" name="id_ope_pr" id="id_ope_pr" value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->IDMaterial); ?>">
            <div class="modal-body">
                <div class="col-md-12">      
                            <div class="row">
                                        <div class="col-md-5">
                                                <div class="form-group">
                                                                <label>Fecha Operacion:</label>
                                                                <div class="input-group date">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <input required type="text" class="form-control pull-right" id="datepicker" name="datepicker">
                                                                </div>
                                                    </div> 
                                         </div>           
                            </div>
                            <div class="row">
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                                <label>Usuarios:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="usuario_ope_pr" name="usuario_ope_pr" >
                                                                        <option value="" selected>Seleccionar</option>
                                                                        <?php $valuetipo=0;
                                                                        foreach($usuario as $registro):?>
                                                                            <?php if($registro->ccasevalue == $valuetipo):?>
                                                                            <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                        <?php else:?>
                                                                            <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                            <?php endif;?>
                                                                        <?php endforeach;?>
                                                                </select>  
                                                    </div>  
                                        </div>
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                                <label>Ejemplares disponibles:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="ejemplar_ope_pr" name="ejemplar_ope_pr" >
                                                                        <option value="" selected>Seleccionar</option>
                                                                        <?php $valuetipo=0;
                                                                        foreach($ejemplar_prestar as $registro):?>
                                                                            <?php if($registro->ccasevalue == $valuetipo):?>
                                                                            <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                        <?php else:?>
                                                                            <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                            <?php endif;?>
                                                                        <?php endforeach;?>
                                                                </select>  
                                                    </div>  
                                        </div>     
                            </div>    
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-success" ><span class="fa  fa-check"></span> Procesar</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>



<div class="modal fade" id="modal-operacion_d">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realizar operacion dovolucion</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/procesar_devuelve" method="POST">
            <input  type="hidden" name="id_ope_dv" id="id_ope_dv" value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->IDMaterial); ?>">
            <div class="modal-body">
                <div class="col-md-12">      
                            <div class="row">
                                        <div class="col-md-5">
                                                <div class="form-group">
                                                                <label>Fecha Operacion:</label>
                                                                <div class="input-group date">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <input required type="text" class="form-control pull-right" id="datepicker1" nem="datepicker1">
                                                                </div>
                                                    </div> 
                                         </div>   
                                         <div class="col-md-5">
                                                    <div class="form-group">
                                                                <label>Ejemplares prestados:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="usuario_ope_dv" name="usuario_ope_dv" >
                                                                        <option value="" selected>Seleccionar</option>
                                                                        <?php $valuetipo=0;
                                                                        foreach($ejemplar_devolver as $registro):?>
                                                                            <?php if($registro->ccasevalue == $valuetipo):?>
                                                                            <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                        <?php else:?>
                                                                            <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                            <?php endif;?>
                                                                        <?php endforeach;?>
                                                                </select>  
                                                    </div>  
                                        </div>             
                            </div>
                            <div class="row">
                                        
                                        
                            </div>    
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-success" ><span class="fa  fa-check"></span> Procesar</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-operacion_i">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realizar operacion intercambio</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/procesar" method="POST">
            <div class="modal-body">
                <div class="col-md-12">      
                            <div class="row">
                                        <div class="col-md-5">
                                                <div class="form-group">
                                                                <label>Fecha Operacion:</label>
                                                                <div class="input-group date">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <input type="text" class="form-control pull-right" id="datepicker2">
                                                                </div>
                                                    </div> 
                                         </div>           
                            </div>
                            <div class="row">
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                                <label>Usuarios:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="usuario_ope" name="usuario_ope" >
                                                                        <option value="" selected>Seleccionar</option>
                                                                        <?php $valuetipo=0;
                                                                        foreach($usuario as $registro):?>
                                                                            <?php if($registro->ccasevalue == $valuetipo):?>
                                                                            <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                        <?php else:?>
                                                                            <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                            <?php endif;?>
                                                                        <?php endforeach;?>
                                                                </select>  
                                                    </div>  
                                        </div>
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                                <label>Ejemplares disponibles:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="usuario_ope" name="usuario_ope" >
                                                                        <option value="" selected>Seleccionar</option>
                                                                        <?php $valuetipo=0;
                                                                        foreach($ejemplar as $registro):?>
                                                                            <?php if($registro->ccasevalue == $valuetipo):?>
                                                                            <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                        <?php else:?>
                                                                            <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                            <?php endif;?>
                                                                        <?php endforeach;?>
                                                                </select>  
                                                    </div>  
                                        </div>     
                            </div>    
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-success" ><span class="fa  fa-check"></span> Procesar</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>