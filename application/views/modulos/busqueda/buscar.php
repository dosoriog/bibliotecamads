<?php if (!isset($textbusqueda)){
     $textbusqueda = "";
} ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box ">
            
            <div class="box-header   ">
                    <h3 class="box-title">Buscar</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                        </div>
            </div>

        <div class="box-body">
                        <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/exec_busqueda" method="POST">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if($this->session->flashdata("error")):?>
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                                            </div>
                                        <?php endif;?>
                                        <?php if($this->session->flashdata("result")):?>
                                            <div class="alert alert-success alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("result"); ?></p>
                                            </div>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center">
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" placeholder="Buscar en la biblioteca" id="textbusqueda" name="textbusqueda" value="<?php echo (is_null($textbusqueda)?NULL:$textbusqueda); ?>">
                                                <span class="input-group-btn">
                                                <button type="submit" class="btn btn-info btn-flat">Ir!</button>
                                                </span>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" class="flat-red" checked>
                                                &nbsp;Por Autor&nbsp;&nbsp;&nbsp;&nbsp;
                                            </label>
                                            <label>
                                                <input type="checkbox" class="flat-red">
                                                &nbsp;Por Coleccion&nbsp;&nbsp;&nbsp;&nbsp;
                                            </label>
                                            <label>
                                                <input type="checkbox" class="flat-red" disabled>
                                                &nbsp;Por Institucion
                                            </label>
                                        </div>
                                    </div>  
                                </div>
                        </form>

                        <div class="row">
                            <div class="col-md-12">
                                  <?php if(!empty($registro_dat)):?>
                                    <table id="listregistros" class="table table-striped table-condensed table-bordered">
                                            <thead>
                                                <tr>
                                                    <!-- Inicio Agregar solo las columnas que se quieran mostrar -->
                                                    
                                        
                                                </tr>
                                            </thead>
                                            <tbody>
                                        <?php foreach($registro_dat as $registro_dat):?>
                                            <tr><td><div class="row invoice-info">
                                                    <div class="col-sm-6 invoice-col">
                                                    <h4><?php echo $registro_dat->Institucion; ?>,<?php echo $registro_dat->Anyo." - ".$registro_dat->Titulo; ?></h4>
                                                            <address>
                                                                <?php echo $registro_dat->Imprenta; ?><br>
                                                                <?php echo $registro_dat->ISBN; ?><br>
                                                                <?php echo $registro_dat->ISSN; ?><br>
                                                                <?php echo $registro_dat->Hipervinvulo; ?>
                                                            </address>
                                                            <?php if(($this->session->userdata("rol")==4)){ ?><button type="button" class="btn btn-primary btn-sm" onclick="exec_busqueda('<?php echo $registro_dat->IDMaterial; ?>')"><span class="fa  fa-eye"></span>&nbsp;&nbsp; Ver</button><?php } ?> 
                                                            <button type="button" class="btn btn-warning btn-sm" onclick="ver('<?php echo $registro_dat->IDMaterial; ?>','<?php echo $registro_dat->Titulo; ?>')"><span class="fa  fa-hand-paper-o"></span>&nbsp;&nbsp; Solictar Prestamo</button>
                                                            <?php if(($this->session->userdata("rol")==2)){ ?><button type="button" class="btn btn-primary btn-sm" onclick="ver1('<?php echo $registro_dat->IDMaterial; ?>','<?php echo $registro_dat->Titulo; ?>')"><span class="fa  fa-exchange"></span>&nbsp;&nbsp; Solicitar Canje</button><?php } ?> 
                                                    </div>
                                             </div></td></tr>        
                                        <?php endforeach;?>
                                        </tbody>
                                        </table>
                                  <?php endif;?>
                            </div>  
                        </div>
        </div>
						        
	</div>                
    </section>
    <!-- /.content -->
</div>

<form id="FormBuscarEntrada" action="<?php echo base_url();?>index.php/generales/entradas/buscar" method="POST">
    <input  type="hidden" name="idbuscar" id="idbuscar" value="">
</form>


<!-- /.content-wrapper -->
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Buscar...</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/buscar" method="POST">
            <div class="modal-body">
                <div class="col-md-12">      
                            <div class="row">
                                    
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                    <label for="nombre">ID:</label>
                                                    <input required maxlength="10"  type="text" class="form-control" id="idbuscar" name="idbuscar">
                                            </div>    
                                        </div>
                            </div>
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" ><span class="fa  fa-search"></span> Buscar</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<form id="frmver" action="<?php echo base_url();?>index.php/generales/entradas/solicitar_prestamo" method="POST">
    <input type="hidden" class="form-control" id="idlista" name="idlista" value="">
    <input type="hidden" class="form-control" id="idmaterial" name="idmaterial" value="">
</form>
<?php if(($this->session->userdata("rol")==2)){ ?>
        <form id="frmver1" action="<?php echo base_url();?>index.php/generales/entradas/solicitar_canje" method="POST">
            <input type="hidden" class="form-control" id="idlista_canje" name="idlista_canje" value="">
            <input type="hidden" class="form-control" id="idmaterial_canje" name="idmaterial_canje" value="">
        </form>
<?php } ?>        