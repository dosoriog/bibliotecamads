
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php 
            $estadocontrol = "";
            if($accion=="prestamo"){
                $estadocontrol = "disabled";
            }
        
        ?>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box ">
            
            <div class="box-header   ">
                    <h3 class="box-title">Usuarios</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                        </div>
            </div>

        <div class="box-body">
                        <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/<?php echo $accion;?>" method="POST">
                                <input  type="hidden" name="id" id="id" value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->IDUsuario); ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if($this->session->flashdata("error")):?>
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                                            </div>
                                        <?php endif;?>
                                        <?php if($this->session->flashdata("result")):?>
                                            <div class="alert alert-success alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("result"); ?></p>
                                            </div>
                                        <?php endif;?>
                                   <div class="row">
                                      <div class="col-md-8">      
                                                            <div class="row">
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                    <label for="nombre">ID:</label>
                                                                                    <input  maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->IDUsuario); ?>" type="text" class="form-control" disabled>
                                                                            </div>    
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <div class="form-group">
                                                                                    <label for="nombre">Nombre:</label>
                                                                                    <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Nombre); ?>" type="text" class="form-control" id="nombre" name="nombre" <?php echo $estadocontrol;?>>
                                                                            </div>    
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                                    <div class="form-group">
                                                                                            <label for="nombre"># Documento:</label>
                                                                                            <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Documento); ?>" type="text" class="form-control" id="documento" name="documento" <?php echo $estadocontrol;?>>
                                                                                    </div>    
                                                                        </div>
                                                            </div>
                                                            <div class="row">
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                        <label>Tipo:</label>
                                                                                        <select autocomplete="off" required class="form-control select2" style="width: 100%;" id="tipo" name="tipo" <?php echo $estadocontrol;?>>
                                                                                                <?php if(!empty($registros)){ ?>
                                                                                                        <option value="" >Seleccionar</option>
                                                                                                        <option value="4" <?php if($registro_dat->IdTipoUsuario==4){ ?>selected<?php } ?>>Administrador</option>
                                                                                                        <option value="2" <?php if($registro_dat->IdTipoUsuario==2){ ?>selected<?php } ?>>Institucion</option>
                                                                                                        <option value="3" <?php if($registro_dat->IdTipoUsuario==3){ ?>selected<?php } ?>>Operador</option>
                                                                                                        <option value="1" <?php if($registro_dat->IdTipoUsuario==1){ ?>selected<?php } ?>>Visitante</option>
                                                                                                <?php } else { ?>  
                                                                                                    <option value="" selected>Seleccionar</option>
                                                                                                        <option value="4">Administrador</option>
                                                                                                        <option value="2">Institucion</option>
                                                                                                        <option value="3">Operador</option>
                                                                                                        <option value="1">Visitante</option>  
                                                                                                <?php } ?>            
                                                                                        </select>
                                                                                    </div>   
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                    <div class="form-group">
                                                                                            <label for="nombre">Usuario:</label>
                                                                                            <input autocomplete="off" required maxlength="50"  value="<?php echo (is_null($registro_dat)?NULL:$registro_dat->Usuario); ?>" type="text" class="form-control" id="usuario" name="usuario" <?php echo $estadocontrol;?>>
                                                                                    </div>    
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                    <div class="form-group">
                                                                                            <label for="nombre">Contraseña:</label>
                                                                                            <input autocomplete="off" required maxlength="50"  value="" type="text" class="form-control" id="clave" name="clave" <?php echo $estadocontrol;?>>
                                                                                    </div>    
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                    <div class="form-group">
                                                                                            <label for="nombre">Estado:</label>
                                                                                            <select autocomplete="off" required class="form-control select2" style="width: 100%;" id="estado" name="estado" <?php echo $estadocontrol;?>>
                                                                                                <option value="" selected>Seleccionar</option>
                                                                                                <option value="1" selected>Activo</option>
                                                                                                <option value="0" selected>Inactivo</option>
                                                                                           </select>
                                                                                    </div>    
                                                                            </div>
                                                            </div>
                                                            
                                                            
                                                            
                                      </div>
                                      
                                    </div>

                                        
                                         
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default" ><span class="fa  fa-search"></span> Buscar</button>
                                                    <?php if(($this->session->userdata("rol")==4) || ($this->session->userdata("rol")==3)){ ?>
                                                            <button type="submit" class="btn btn-success"><span class="fa  fa-check"></span> Guardar</button>
                                                            <a class="btn btn-warning" href="<?php echo base_url();?>index.php/generales/entradas/indexuser" role="button"><span class="fa fa-plus"></span> Nuevo</a>      
                                                    <?php }  ?>              
                                                       
                                    </div>
                                </div>
                            
                        </form>
        </div>
						        
	</div>                
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Buscar...</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/buscaruser" method="POST">
            <div class="modal-body">
                <div class="col-md-12">      
                            <div class="row">
                                    
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                    <label for="nombre">ID:</label>
                                                    <input required maxlength="10"  type="text" class="form-control" id="idbuscar" name="idbuscar">
                                            </div>    
                                        </div>
                            </div>
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" ><span class="fa  fa-search"></span> Buscar</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
