<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php 
            $estadocontrol = "";
            if($accion=="prestamo"){
                $estadocontrol = "disabled";
            }
        
        ?>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box ">
            
            <div class="box-header   ">
                    <h3 class="box-title">Operaciones</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                        </div>
            </div>

                    <div class="box-body">
                            
                            <div class="row">
                                <div class="col-md-12">
                                        
                                        <?php if($this->session->flashdata("error")):?>
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                                            </div>
                                        <?php endif;?>
                                        <?php if($this->session->flashdata("result")):?>
                                            <div class="alert alert-success alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("result"); ?></p>
                                            </div>
                                        <?php endif;?>

                                        <table id="listregistros" class="table table-striped table-condensed table-bordered">
                                            <thead>
                                                <tr>
                                                    <!-- Inicio Agregar solo las columnas que se quieran mostrar -->
                                                    <th>ID</th>
                                                    <th>Titulo</th>
                                                    <th>Ejemplar</th>
                                                    <th>Tipo</th>
                                                    <th>Usuario</th>
                                                    <th>Fecha Solicitud</th>
                                                    <th>Fecha Prestamo</th>
                                                    <th>Usuario Presta</th>
                                                    <th>Operacion</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(!empty($registro_dat)):?>
                                                    <?php foreach($registro_dat as $registro):?>
                                                        <tr> 
                                                            <td><?php echo $registro->IdPrestamo;?></td>
                                                            <td><?php echo $registro->IDMaterial." - ".$registro->Titulo;?></td>
                                                            <td><?php echo $registro->Codigo;?></td>
                                                            <td><?php if($registro->Intercambio=="NO"){ echo "Prestamo"; } elseif($registro->Intercambio=="SI") { echo "Canje"; }?></td>
                                                            <td><?php echo $registro->Nombre;?></td>
                                                            <td><?php echo $registro->Fechasolicitud;?></td>
                                                            <td><?php echo $registro->Fechaprestamo;?></td>
                                                            <td><?php echo $registro->UPresta;?></td>
                                                            <td><div class="form-group"><?php if($registro->Estado==4){ ?><button type="button" class="btn btn-primary btn-sm"  onclick="prestar('<?php echo $registro->Idusuario; ?>','<?php echo $registro->Titulo; ?>','<?php echo $registro->Fechasolicitud; ?>','<?php echo $registro->IdPrestamo; ?>','<?php echo $registro->IDMaterial; ?>')"><span class="fa  fa-book"></span> Prestar</button><?php } 
                                                                      elseif($registro->Estado==1002) { ?><button type="button" class="btn btn-warning btn-sm" onclick="devolver('<?php echo $registro->Idusuario; ?>','<?php echo $registro->Titulo; ?>','<?php echo $registro->Fechasolicitud; ?>','<?php echo $registro->IdPrestamo; ?>','<?php echo $registro->IDMaterial; ?>','<?php echo $registro->Fechaprestamo; ?>','<?php echo $registro->Codigo; ?>')"><span class="fa  fa-book"></span> Devolver</button><?php }
                                                                      elseif($registro->Estado==1003) { ?><button type="button" class="btn btn-success btn-sm"  onclick="canje('<?php echo $registro->Idusuario; ?>','<?php echo $registro->Titulo; ?>','<?php echo $registro->Fechasolicitud; ?>','<?php echo $registro->IdPrestamo; ?>','<?php echo $registro->IDMaterial; ?>')"><span class="fa  fa-book"></span> Canje</button><?php }
                                                                     ?><button type="button" class="btn btn-danger btn-sm" onclick="anular('<?php echo $registro->Idusuario; ?>','<?php echo $registro->Titulo; ?>','<?php echo $registro->Fechasolicitud; ?>','<?php echo $registro->IdPrestamo; ?>','<?php echo $registro->IDMaterial; ?>','<?php echo $registro->Fechaprestamo; ?>','<?php echo $registro->Codigo; ?>')"><span class="fa  fa-book"></span> Anular</button><?php 
                                                                ?>
                                                            </div></td>
                                                        </tr>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </tbody>
                                        </table>         
                                    </div>       
                                </div>   

                    </div>
        </div>    
  </section>
    <!-- /.content -->
</div>       
<form id="frmbuscar_ejemplar" action="<?php echo base_url();?>index.php/generales/entradas/buscar_ejemplares" method="POST">
    <input type="hidden" class="form-control" id="idMaterial_busc" name="idMaterial_busc" value="">
</form>
<div class="modal fade" id="modal-operacion_p">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realizar operacion prestamos</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/procesar_prestamo" method="POST">
            <input  type="hidden" name="id_ope_pr" id="id_ope_pr" value="">
            <div class="modal-body">
                <div class="col-md-12"> 
                            <br />
                            <div class="row">  
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>ID:&nbsp;&nbsp;</strong><span id="html1"></span>
                                                                </div>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-7">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>Titulo:&nbsp;&nbsp;</strong><span id="html2"></span>
                                                                </div>
                                                        </div>
                                                    </div>  
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>Fecha:&nbsp;&nbsp;</strong><span id="html3"></span>
                                                                </div>
                                                        </div>
                                                    </div>   
                            </div>    
                            <div class="row">
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                                <label>Usuarios:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="usuario_ope_pr" name="usuario_ope_pr" >
                                                                        <option value="" selected>Seleccionar</option>
                                                                        <?php $valuetipo=0;
                                                                        foreach($usuario as $registro):?>
                                                                            <?php if($registro->ccasevalue == $valuetipo):?>
                                                                            <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                        <?php else:?>
                                                                            <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                            <?php endif;?>
                                                                        <?php endforeach;?>
                                                                </select>  
                                                    </div>  
                                        </div>
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                                <label>Ejemplares disponibles:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="ejemplar_ope_pr" name="ejemplar_ope_pr" >
                                                                        
                                                                </select>  
                                                    </div>  
                                        </div>     
                            </div>    
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-success" ><span class="fa  fa-check"></span> Procesar</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-operacion_d">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realizar operacion devolucion</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/procesar_devuelve" method="POST">
            <input  type="hidden" name="id_ope_dv" id="id_ope_dv" value="">
            <div class="modal-body">
                <div class="col-md-12">      
                <br />
                            <div class="row">  
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>ID:&nbsp;&nbsp;</strong><span id="html1_d"></span>
                                                                </div>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>Titulo:&nbsp;&nbsp;</strong><span id="html2_d"></span>
                                                                </div>
                                                        </div>
                                                    </div>  
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>Fecha solicitud:&nbsp;&nbsp;</strong><span id="html3_d"></span>
                                                                </div>
                                                        </div>
                                                    </div>  
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>Fecha prestamo:&nbsp;&nbsp;</strong><span id="html4_d"></span>
                                                                </div>
                                                        </div>
                                                    </div>    
                            </div>    
                            <div class="row">
                                        <div class="col-md-4">
                                                                <div class="form-group">
                                                                <label>Usuarios:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="usuario_ope_dv" name="usuario_ope_dv" >
                                                                        <option value="" selected>Seleccionar</option>
                                                                        <?php $valuetipo=0;
                                                                        foreach($usuario as $registro):?>
                                                                            <?php if($registro->ccasevalue == $valuetipo):?>
                                                                            <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                        <?php else:?>
                                                                            <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                            <?php endif;?>
                                                                        <?php endforeach;?>
                                                                </select>  
                                                    </div>  
                                        </div>
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                                <label>Ejemplar:</label>
                                                                <input required maxlength="50"  value="" type="text" class="form-control" id="ejemplar_ope_dv" name="ejemplar_ope_dv" disabled>
                                                    </div>  
                                        </div>     
                            </div>    
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-success" ><span class="fa  fa-check"></span> Procesar</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-operacion_a">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realizar operacion anular</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/procesar_anular" method="POST">
            <input  type="hidden" name="id_ope_an" id="id_ope_an" value="">
            <div class="modal-body">
                <h4>¿Confirma que desea anular la operación actual?</h4>
                <div class="col-md-12">      
                <br />
                            <div class="row">  
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>ID:&nbsp;&nbsp;</strong><span id="html1_a"></span>
                                                                </div>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>Titulo:&nbsp;&nbsp;</strong><span id="html2_a"></span>
                                                                </div>
                                                        </div>
                                                    </div>  
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>Fecha solicitud:&nbsp;&nbsp;</strong><span id="html3_a"></span>
                                                                </div>
                                                        </div>
                                                    </div>  
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>Fecha prestamo:&nbsp;&nbsp;</strong><span id="html4_a"></span>
                                                                </div>
                                                        </div>
                                                    </div>    
                            </div>    
                            <div class="row">
                                        <div class="col-md-4">
                                                                <div class="form-group">
                                                                <label>Usuarios:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="usuario_ope_an" name="usuario_ope_an" >
                                                                        <option value="" selected>Seleccionar</option>
                                                                        <?php $valuetipo=0;
                                                                        foreach($usuario as $registro):?>
                                                                            <?php if($registro->ccasevalue == $valuetipo):?>
                                                                            <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                        <?php else:?>
                                                                            <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                            <?php endif;?>
                                                                        <?php endforeach;?>
                                                                </select>  
                                                    </div>  
                                        </div>
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                                <label>Ejemplar:</label>
                                                                <input required maxlength="50"  value="" type="text" class="form-control" id="ejemplar_ope_an" name="ejemplar_ope_an" disabled>
                                                    </div>  
                                        </div>     
                            </div>    
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-success" ><span class="fa  fa-check"></span> Anular</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<form id="frmbuscar_ejemplar_canje" action="<?php echo base_url();?>index.php/generales/entradas/buscar_ejemplares_canje" method="POST">
    <input type="hidden" class="form-control" id="idMaterial_busc_canje" name="idMaterial_busc_canje" value="">
</form>
<div class="modal fade" id="modal-operacion_cj">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realizar operacion canje</h4>
      </div>
      <form id="FormDatReg" action="<?php echo base_url();?>index.php/generales/entradas/procesar_canje" method="POST">
            <input  type="hidden" name="id_ope_cj" id="id_ope_cj" value="">
            <div class="modal-body">
                <div class="col-md-12"> 
                            <br />
                            <div class="row">  
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>ID:&nbsp;&nbsp;</strong><span id="html1_c"></span>
                                                                </div>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-7">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>Titulo:&nbsp;&nbsp;</strong><span id="html2_c"></span>
                                                                </div>
                                                        </div>
                                                    </div>  
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                                <div class="input-group date">
                                                                        <strong>Fecha:&nbsp;&nbsp;</strong><span id="html3_c"></span>
                                                                </div>
                                                        </div>
                                                    </div>   
                            </div>    
                            <div class="row">
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                                <label>Usuarios:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="usuario_ope_cj" name="usuario_ope_cj" >
                                                                        <option value="" selected>Seleccionar</option>
                                                                        <?php $valuetipo=0;
                                                                        foreach($usuario as $registro):?>
                                                                            <?php if($registro->ccasevalue == $valuetipo):?>
                                                                            <option value="<?php echo $registro->ccasevalue?>" selected><?php echo $registro->ccasenombre;?></option>
                                                                        <?php else:?>
                                                                            <option value="<?php echo $registro->ccasevalue?>"><?php echo $registro->ccasenombre;?></option>
                                                                            <?php endif;?>
                                                                        <?php endforeach;?>
                                                                </select>  
                                                    </div>  
                                        </div>
                                        <div class="col-md-4">
                                                    <div class="form-group">
                                                                <label>Ejemplares disponibles:</label>
                                                                <select required class="form-control select2" style="width: 100%;"  id="ejemplar_ope_cj" name="ejemplar_ope_cj" >
                                                                        
                                                                </select>  
                                                    </div>  
                                        </div>     
                            </div>    
                </div>                   
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-success" ><span class="fa  fa-check"></span> Procesar</button>  
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
      </form>      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>