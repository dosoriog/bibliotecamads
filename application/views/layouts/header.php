<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Biblioteca</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="icon" type="image/png" href="" /> 
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/bootstrap/css/bootstrap.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/vendors/selectFX/css/cs-skin-elastic.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/iCheck/all.css">
    <!-- <link rel="stylesheet" href="assets/template/assets/css/style.css">-->

    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/select2/dist/css/select2.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
    .carousel-inner.vertical {
height: 100%; /*Note: set specific height here if not, there will be some issues with IE browser*/
}
.carousel-inner.vertical > .item {
-webkit-transition: .6s ease-in-out top;
-o-transition: .6s ease-in-out top;
transition: .6s ease-in-out top;
}

@media all and (transform-3d),
(-webkit-transform-3d) {
.carousel-inner.vertical > .item {
  -webkit-transition: -webkit-transform .6s ease-in-out;
  -o-transition: -o-transform .6s ease-in-out;
  transition: transform .6s ease-in-out;
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
  -webkit-perspective: 1000;
  perspective: 1000;
}
.carousel-inner.vertical > .item.next,
.carousel-inner.vertical > .item.active.right {
  -webkit-transform: translate3d(0, 33.33%, 0);
  transform: translate3d(0, 33.33%, 0);
  top: 0;
}
.carousel-inner.vertical > .item.prev,
.carousel-inner.vertical > .item.active.left {
  -webkit-transform: translate3d(0, -33.33%, 0);
  transform: translate3d(0, -33.33%, 0);
  top: 0;
}
.carousel-inner.vertical > .item.next.left,
.carousel-inner.vertical > .item.prev.right,
.carousel-inner.vertical > .item.active {
  -webkit-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);
  top: 0;
}
}

.carousel-inner.vertical > .active {
top: 0;
}
.carousel-inner.vertical > .next,
.carousel-inner.vertical > .prev {
top: 0;
height: 100%;
width: auto;
}
.carousel-inner.vertical > .next {
left: 0;
top: 33.33%;
right:0;
}
.carousel-inner.vertical > .prev {
left: 0;
top: -33.33%;
right:0;
}
.carousel-inner.vertical > .next.left,
.carousel-inner.vertical > .prev.right {
top: 0;
}
.carousel-inner.vertical > .active.left {
left: 0;
top: -33.33%;
right:0;
}
.carousel-inner.vertical > .active.right {
left: 0;
top: 33.33%;
right:0;
}

#carousel-example-generic .carousel-control.left {
  bottom: initial;
  width: 100%;
}
#carousel-example-generic .carousel-control.right {
  top: initial;
  width: 100%;
}
body {
        padding-right: 0 !important;
     }
</style>
</head>
<body class="hold-transition wysihtml5-supported fixed skin-blue-light sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?php echo base_url();?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>B</b>Teca</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Biblioteca</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo base_url()?>assets/template/dist/img/avatar5.png" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo $this->session->userdata("nombre")." (".$this->session->userdata("usuario").")"; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-body">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <a href="<?php echo base_url()?>index.php/auth/logout"> Cerrar Sesión</a>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
