
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> <?php echo $this->config->item('importe_version'); ?>
            </div>
            <strong>Copyright &copy; <?php echo $this->config->item('importe_ayyo'); ?> <a href="<?php echo $this->config->item('wwwimporte_link'); ?>" target="_blank"><?php echo $this->config->item('importe_nombre'); ?></a>.</strong> All rights
            reserved.
        </footer>
    </div>
<script src="<?php echo base_url();?>assets/template/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/template/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/template/validate/jquery.validate.js"></script>
<script src="<?php echo base_url();?>assets/template/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/template/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/template/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/template/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?php echo base_url();?>assets/template/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url();?>assets/template/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/template/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/template/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url();?>assets/template/fastclick/lib/fastclick.js"></script>
<script src="<?php echo base_url();?>assets/template/dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url();?>assets/template/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template/numerosletras.js"></script>

<!--<script src="assets/template/js_upload/xls.js"></script>
<script src="assets/template/js_upload/readXLS.js"></script>-->
<!-- <script src="<?php echo base_url();?>assets/template/vendors/popper.js/dist/umd/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/template/assets/js/main.js"></script>-->
<script>
   var nom_controller = "<?php echo $this->config->item("nom_controller");?>";
   var accion_act     = "<?php echo $this->session->userdata("accion_act");?>";
   var view_act       = "<?php echo $this->session->userdata("view_act");?>";
   var base_url       = "<?php echo base_url().$this->config->item('url_controller');?>";
   var base_url1      = "<?php echo base_url();?>";
   var format_fechas  = "<?php echo $this->session->userdata("format_fechas");?>";
   var signo_miles    = "<?php echo $this->session->userdata("signo_miles"); ?>";
   var btn_tamano     = "<?php echo $this->config->item('btn_tamano'); ?>";
   var msj_noexistencias = "<?php echo "El producto actual no tiene existencias en inventario."; ?>";

   var base_ctr = "<?php echo $this->config->item('nom_controller');?>";
   var base_act = "<?php echo $this->session->userdata("accion_act");?>";
</script>
<script src="<?php echo base_url();?>assets/template/validate/eventos_importeapp.js"></script>


<?php if(in_array($this->session->userdata("view_act"), $this->config->item('list_indexcontroller'))):?>
     <script src="<?php echo base_url()."assets/template/validate/module/".$this->config->item('url_controller_view')."document_ready.js"; ?>"></script>
<?php endif;?>

<?php if($this->session->userdata("accion_act") != 'index'):?>
     <script src="<?php echo base_url()."assets/template/validate/module/".$this->config->item('url_controller_view')."document_ready_viewreg.js"; ?>"></script>
<?php endif;?>

<?php if($this->session->userdata("accion_act") == 'index'):?>
   <script src="<?php echo base_url()."assets/template/validate/module/".$this->config->item('url_controller_view')."script_index.js"; ?>"></script>
<?php endif;?>

<?php if($this->session->userdata("accion_act") != 'index'):?>
    <script src="<?php echo base_url()."assets/template/validate/module/".$this->config->item('url_controller_view')."script_view_reg.js"; ?>"></script>
    <script>
        $(document).ready(function () {
          $.getScript('<?php echo base_url()."assets/template/validate/module/".$this->config->item('url_controller_view')."validacion.js"; ?>', function(){
          });
        })
    </script>
<?php endif;?>

<script>
<?php if(($this->config->item("nom_controller")=='aperturascajas') && ($this->session->userdata("view_act")=='aperturascajasmovimientocaja')):?>


<?php endif;?>
/*$(document).ready(function () {
    
  $('#listregistros').DataTable({
        // "columnDefs": [{ className: "text-right", targets: [ 3, 4 ] },{ className: "text-center", targets: [ 0, 1, 5, 6 ] }],
         "ordering"    : false,
         'paging'      : true,
         'lengthChange': false,
         'searching'   : false,
         'info'        : true,
         'autoWidth'   : false,
         "language": {
             "lengthMenu": "Mostrar _MENU_ registros por pagina",
             "zeroRecords": "No se encontraron resultados en su busqueda",
             "searchPlaceholder": "Buscar registros",
             "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
             "infoEmpty": "No existen registros",
             "infoFiltered": "(filtrado de un total de _MAX_ registros)",
             "search": "Buscar:",
             "paginate": {
                 "first": "Primero",
                 "last": "Último",
                 "next": "Siguiente",
                 "previous": "Anterior"
             },
         }
     });


});*/

function ver(id,nom){
      $("#idlista").val(id);
      $("#idmaterial").val(nom);
      $('#frmver').submit();
}

function ver1(id,nom){
      $("#idlista_canje").val(id);
      $("#idmaterial_canje").val(nom);
      $('#frmver1').submit();
}

function exec_busqueda(id){
      $("#idbuscar").val(id);
      $('#FormBuscarEntrada').submit();
}

function prestar(iduser,titulo,fecha,idoperacion,idmaterial){
  $("#idMaterial_busc").val(idmaterial);
  //alert($("#idMaterial_busc").val());
  $.ajax({
          type: $("#frmbuscar_ejemplar").attr("method"),
          url: $("#frmbuscar_ejemplar").attr("action"),
          data:$("#frmbuscar_ejemplar").serialize(),
          beforeSend: function(){
          },
          complete:function(data){
          },
          success: function(data){
                 // alert(data);
                  $('#usuario_ope_pr').val(iduser).trigger('change.select2');
                  $('#id_ope_pr').val(idoperacion);
                  $("#html1").html(idoperacion);
                  $("#html2").html(titulo);
                  $("#html3").html(fecha);
                  $("#ejemplar_ope_pr").html(data);
                  $('#usuario_ope_pr').prop('disabled', true);
                  $("#modal-operacion_p").modal('show');
          },
          error: function(data){
              alert(data);
              alert("Problemas al tratar de enviar el formulario");
          }
      });
}

function devolver(iduser,titulo,fecha,idoperacion,idmaterial,fechap,ejemplar){
        $('#usuario_ope_dv').val(iduser).trigger('change.select2');
        $('#usuario_ope_dv').prop('disabled', true);
        $('#ejemplar_ope_dv').val(ejemplar);
        $('#id_ope_dv').val(idoperacion);
        $("#html1_d").html(idoperacion);
        $("#html2_d").html(titulo);
        $("#html3_d").html(fecha);
        $("#html4_d").html(fechap);
        $("#modal-operacion_d").modal('show'); 
}

function anular(iduser,titulo,fecha,idoperacion,idmaterial,fechap,ejemplar){
        $('#usuario_ope_an').val(iduser).trigger('change.select2');
        $('#usuario_ope_an').prop('disabled', true);
        $('#ejemplar_ope_an').val(ejemplar);
        $('#id_ope_an').val(idoperacion);
        $("#html1_a").html(idoperacion);
        $("#html2_a").html(titulo);
        $("#html3_a").html(fecha);
        $("#html4_a").html(fechap);
        $("#modal-operacion_a").modal('show'); 
}

function canje(iduser,titulo,fecha,idoperacion,idmaterial){
  $("#idMaterial_busc_canje").val(idmaterial);
  $.ajax({
          type: $("#frmbuscar_ejemplar_canje").attr("method"),
          url: $("#frmbuscar_ejemplar_canje").attr("action"),
          data:$("#frmbuscar_ejemplar_canje").serialize(),
          beforeSend: function(){
          },
          complete:function(data){
          },
          success: function(data){
                  //alert(data);
                  $('#usuario_ope_cj').val(iduser).trigger('change.select2');
                  $('#id_ope_cj').val(idoperacion);
                  $("#html1_c").html(idoperacion);
                  $("#html2_c").html(titulo);
                  $("#html3_c").html(fecha);
                  $("#ejemplar_ope_cj").html(data);
                  $('#usuario_ope_cj').prop('disabled', true);
                  $("#modal-operacion_cj").modal('show');
          },
          error: function(data){
              alert(data);
              alert("Problemas al tratar de enviar el formulario");
          }
      });
}
</script>

<script>

<?php if(($this->config->item("nom_controller")=='aperturascajas') && ($this->session->userdata("view_act")=='aperturascajasmovimientocaja')):?>

<?php endif;?>

//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
  checkboxClass: 'icheckbox_minimal-blue',
  radioClass   : 'iradio_minimal-blue'
})
//Red color scheme for iCheck
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
  checkboxClass: 'icheckbox_minimal-red',
  radioClass   : 'iradio_minimal-red'
})
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
  checkboxClass: 'icheckbox_flat-green',
  radioClass   : 'iradio_flat-green'
})

$('.select2').select2();
$('.sidebar-menu').tree();

$('#datepicker').datepicker({
  autoclose: true,
  todayBtn: true,
  showInputs: false,
  todayHighlight: true,
  format: "yyyy-mm-dd",
})

$('#datepicker1').datepicker({
  autoclose: true,
  todayBtn: true,
  showInputs: false,
  todayHighlight: true,
  format: "yyyy-mm-dd",
})

$('#datepicker2').datepicker({
  autoclose: true,
  todayBtn: true,
  showInputs: false,
  todayHighlight: true,
  format: "yyyy-mm-dd",
})


$('.carousel .vertical .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=1;i<2;i++) {
    next=next.next();
    if (!next.length) {
    	next = $(this).siblings(':first');
  	}

    next.children(':first-child').clone().appendTo($(this));
  }
});

</script>

</body>
</html>
