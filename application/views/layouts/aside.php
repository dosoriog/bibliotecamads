<?php

       
              $dir_img = "Clasic";
              $dir_img_upload_inv = "upload";
              $icon_proser_defaul = "prod_serv.png"; // Icono de productos/servicios por defecto
              $icon_categoria_defaul = "categoria_defecto2.png";

              $text_bold_head = " text-bold ";
              $text_color_head = " text-warning "; // text-primary,text-secondary,text-success,text-danger,text-warning,text-info,text-light bg-dark,text-dark,text-muted",text-white bg-dark
              $text_color1_head = " text-primary ";

              $title_menu = "Modulos Activos";

              $label_Inicio = "Inicio";
              $icon_Inicio = "fa-home";

              $label_Generales = "Generales";
              $icon_Generales = "fa-clipboard";

              $label_Personas = "Personas";
              $icon_Personas = "fa-street-view";

              $label_TipoEntidades = "Tipo Entidades";
              $icon_TipoEntidades = "fa-th-list";

              $label_Entidades = "Entidades";
              $icon_Entidades = "fa-th";

              $label_Financiero = "Financiero";
              $icon_Financiero = "fa-calculator";

              $label_Cajas = "Cajas";
              $icon_Cajas = "fa-money";

              $label_CuentasBancarias = "Cuentas Bancarias";
              $icon_CuentasBancarias = "fa-cc-visa";

              $label_NotaBanco = "Concep. Nota/Banco";
              $icon_NotaBanco = "fa-indent";

              $label_Conceptos = "Conceptos";
              $icon_Conceptos = "fa-indent";

              $label_Compras = "Compras";
              $icon_Compras = "fa-clone";

              $label_FacturasCompra = "Facturas Compra";
              $icon_FacturasCompra = "fa-clone";

              $label_CXP = "CXP - Egresos";
              $icon_CXP = "fa-briefcase";

          //    $icon_Ventas = "fa-glass";
              $label_Ventas = "Ventas";
              $icon_Ventas = "fa-copy";

              $label_FacturasVenta = "Facturas de Venta";
              $icon_FacturasVenta = "fa-copy";

              $label_Cotizacion = "Cotizaciones";
              $icon_Cotizacion = "fa-pencil-square-o";

              $label_Directa = "Directa/Punto de Venta";
              $icon_Directa = "fa-shopping-cart";

              $label_Puestos = "Puestos";
              $icon_Puestos = "fa-copy";

              $label_Tesoreria = "Tesoreria";
              $icon_Tesoreria = "fa-dollar";

              $label_CXC = "CXC - Pagos";
              $icon_CXC = " fa-money";

              $label_Apertura = "Apertura";
              $icon_Apertura = "fa-stack-overflow";

              $label_EgresosPagos = "Egresos/Pagos";
              $icon_EgresosPagos = "fa-money";

              $label_Inventarios = "Inventarios";
              $icon_Inventarios = "fa-cubes";

              $label_Bodegas = "Bodegas";
              $icon_Bodegas = "fa-braille";

              $label_TipoMedidas = "Tipo de Medidas";
              $icon_TipoMedidas = "fa-object-group";

              $label_Categorias = "Categorias";
              $icon_Categorias = "fa-object-ungroup";

              $label_Marcas = "Marcas";
              $icon_Marcas = "fa-tags";

              $label_Inventario = "Inventario";
              $icon_Inventario = "fa-cube";

              $label_Entradas = "Entradas";
              $icon_Entradas = "fa-indent";

              $label_Salidas = "Salidas";
              $icon_Salidas = "fa-outdent";

              $label_TelentoHumano = "Telento Humano";
              $icon_TelentoHumano = "fa-user-circle";

              $label_Cargos = "Cargos";
              $icon_Cargos = "fa-black-tie";

              $label_HV = "HV";
              $icon_HV = "fa-circle-o";

              $label_Contratos = "Contratos";
              $icon_Contratos = "fa-handshake-o";

              $label_BarRestaurante = "Bar/Restaurante";
              $icon_BarRestaurante = "fa-cutlery";

              $label_Puestos = "Puestos";
              $icon_Puestos = "fa-spoon";

              $label_Clientes = "Clientes";
              $icon_Clientes = "fa-circle-o";

              $label_Meseros = "Meseros";
              $icon_Meseros = "fa-male";

              $label_ProductosServicios = "Productos/Servicios";
              $icon_ProductosServicios = "fa-spoon";

              $label_CartasMenus = "Cartas/Menus";
              $icon_CartasMenus = "fa-map-o";

              $label_Domicilios = "Domicilios";
              $icon_Domicilios = "fa-motorcycle";

              $label_HVVehiculos = "HV Vehiculos";
              $icon_HVVehiculos = "fa-car";

              $label_Criterios = "Criterios";
              $icon_Criterios = "fa-low-vision";

              $label_Personal = "Personal";
              $icon_Personal = "fa-male";

              $label_Vehiculos = "Vehiculos";
              $icon_Vehiculos = "fa-car";

              $label_Hojasvidav = "Hojas de Vida";
              $icon_Hojasvidav = "fa-folder-open";

              $label_Programador = "Programador";
              $icon_Programador = "fa-calendar";

              $label_Sesiones = "Sesiones";
              $icon_Sesiones = "fa-wrench";

              $label_CargueArchivos = "Upload - Cargar";
              $icon_CargueArchivos = "fa-upload";

              $label_MovimientosCajas = "Cargue de Archivos";
              $icon_MovimientosCajas = "fa-upload";

              $label_Reportes = "Reportes/Indicadores";
              $icon_Reportes = "fa-bar-chart";

              $label_Report_Indicadores = "Indicadores";
              $icon_Report_Indicadores = "fa-line-chart";

              $label_Seguridad = "Seguridad";
              $icon_Seguridad = "fa-lock";

              $label_RolesGrupos = "Roles/Grupos";
              $icon_RolesGrupos = "fa-group";

              $label_Privilegios = "Privilegios";
              $icon_Privilegios = "fa-key";

              $label_Usuarios = "Usuarios";
              $icon_Usuarios = "fa-id-badge";

              $label_Estadisticas = "Estadisticas";
              $icon_Estadisticas = "fa-line-chart";

              $label_Notificaciones = "Notificaciones";
              $icon_Notificaciones = "fa-bullhorn";

              $label_Administrador = "Administrador";
              $icon_Administrador = "fa-cogs";

              $label_Modulos = "Modulos";
              $icon_Modulos = "fa-cube";

              $label_Parametros = "Parametros";
              $icon_Parametros = "fa-wrench";

              $label_Documentos = "Documentos";
              $icon_Documentos = "fa-cube";

              $label_AyudaSoporte = "Ayuda y Soporte";
              $icon_AyudaSoporte = "fa-support";

              $label_Contactenos = "Contactenos";
              $icon_Contactenos = "fa-tty";

              $label_Preguntas = "Preguntas";
              $icon_Preguntas = "fa-question-circle";

              $label_Ayuda = "Ayuda ImporteApp";
              $icon_Ayuda = "fa-book";

              $label_Acercade = "Acerca de...";
              $icon_Acercade = "fa-puzzle-piece";
        

        $data = array(
              'dir_img' => $dir_img,
              'icon_proser_defaul' => $icon_proser_defaul,
              'icon_categoria_defaul' => $icon_categoria_defaul,
              'dir_img_upload_inv' => $dir_img_upload_inv,
        );
        $this->session->set_userdata($data);
?>

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header"><?php echo $title_menu;?></li>
                    <li>
                        <a href="<?php echo base_url();?>">
                            <i class="fa <?php echo $icon_Inicio;?>"></i> <span class=" <?php echo $text_bold_head; ?>  <?php echo $text_color_head; ?> "><?php echo $label_Inicio;?></span>
                        </a>
                    </li>
                    <li class="header <?php if($this->config->item('usarlogoempresa')=="SI"){ echo $this->config->item('color_divhead_emp'); } else{ echo $this->config->item('color_divhead'); } ?>"></li>
                    
                    <li class="treeview  ">
                        <a href="#">
                            <i class="fa <?php echo $icon_Generales;?>"></i> <span class=" <?php echo $text_bold_head; ?>  <?php echo $text_color_head; ?> "><?php echo $label_Generales;?></span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li ><a href="<?php echo base_url();?>index.php/generales/entradas/busqueda"><i class="fa  fa-search"></i> Busqueda</a></li>
                            <?php if(($this->session->userdata("rol")==4)){ ?><li ><a href="<?php echo base_url();?>index.php/generales/entradas"><i class="fa fa-book"></i> Entradas</a></li><?php } ?>
                            <?php if(($this->session->userdata("rol")==4) || ($this->session->userdata("rol")==3)){ ?><li ><a href="<?php echo base_url();?>index.php/generales/entradas/operaciones"><i class="fa fa-folder-open"></i> Operaciones</a></li><?php } ?>
                            <?php if($this->session->userdata("rol")==4){ ?><li ><a href="<?php echo base_url();?>index.php/generales/entradas/indexuser"><i class="fa fa-users"></i> Usuarios</a></li><?php } ?>
                        </ul>
                    </li> 
                    <li class="treeview ">
                        <a href="#">
                            <i class="fa <?php echo $icon_AyudaSoporte;?>"></i> <span class=" <?php echo $text_bold_head; ?>  <?php echo $text_color1_head; ?> "><?php echo $label_AyudaSoporte;?></span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#" target="_blank"><i class="fa <?php echo $icon_Contactenos;?>"></i><?php echo $label_Contactenos;?></a></li>
                            <li><a href="#" target="_blank"><i class="fa <?php echo $icon_Preguntas;?>"></i><?php echo $label_Preguntas;?></a></li>
                            <li><a href="#" target="_blank"><i class="fa <?php echo $icon_Ayuda;?>"></i>Ayuda</a></li>
                            <li><a href="#" target="_blank"><i class="fa <?php echo $icon_Acercade;?>"></i><?php echo $label_Acercade;?></a></li>
                        </ul>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->
